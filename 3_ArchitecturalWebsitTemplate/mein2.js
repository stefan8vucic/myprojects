const projectsContainer = document.getElementById("projectsContainer");
const imagesPreviewContainer = document.getElementById("imagesPreviewContainer");
const previousImage = document.getElementById("previousImage");
const nextImage = document.getElementById("nextImage");

const imagesPreview = document.getElementById("imagesPreview");
const projectsTypeGalery_exterier_1 = document.getElementById("projectsTypeGalery_exterier_1");
const projectsTypeGalery_enterijer_1 = document.getElementById("projectsTypeGalery_enterijer_1");
const projectsTypeGalery_modeling_1 = document.getElementById("projectsTypeGalery_modeling_1");


const closePreview = document.getElementById("closePreview");
const navigation = document.getElementById("navigation");

let index = 0;
let imagesForDisplay;

const projectsTypeGalery_exterier_1_images = [ "./Images/exterier2.jpg", "./Images/exterier10.jpg", "./Images/exterier11.jpg", "./Images/exterier1.jpg"];
const projectsTypeGalery_enterijer_1_images = ["./Images/enterier.jpg", "./Images/enterier1.jpg", "./Images/enterier2.jpg"];
const projectsTypeGalery_modeling_1_images = ["./Images/modeling1.jpg", "./Images/modeling2.jpg", "./Images/modeling3.jpg"];

nextImage.addEventListener("click", () =>{
    index++;
    if(index === imagesForDisplay.length){
        index = 0
    }
    showNextImage(index);
});

const showNextImage = (index) => {
    const imgElement = document.createElement("img");
    imgElement.src = imagesForDisplay[index];
    imgElement.classList.add("nextImage");
    imagesPreview.appendChild(imgElement);

    const img1 = imagesPreview.getElementsByTagName("img")[0];
    img1.classList.add("previouseImage");

    const delete2 = imagesPreview.getElementsByTagName("img")[1];

    setTimeout(() => {
        delete2.classList.remove("nextImage");
        delete2.classList.add("showImage");
    }, 50);

    setTimeout(() => {
        imagesPreview.removeChild(imagesPreview.childNodes[1]);
    }, 200);
    
    handleImagesPreviewSliderScale(index, imagesForDisplay);
}

previousImage.addEventListener("click", () => {
    index--;
    if(index === -1){
        index = imagesForDisplay.length - 1
    };
    showPreviouseImage(index);
});

const showPreviouseImage = (index) => {
    const imgElement = document.createElement("img");
    imgElement.src = imagesForDisplay[index];
    imgElement.classList.add("previouseImage");
    imagesPreview.appendChild(imgElement);

    const img1 = imagesPreview.getElementsByTagName("img")[0];
    img1.classList.add("nextImage");

    const delete2 = imagesPreview.getElementsByTagName("img")[1];

    setTimeout(() => {
        delete2.classList.remove("previouseImage");
        delete2.classList.add("showImage");
    }, 50);

    setTimeout(() => {
        imagesPreview.removeChild(imagesPreview.childNodes[1]);
    }, 300);

    handleImagesPreviewSliderScale(index, imagesForDisplay);
};

displayImagePrewiew = (scroll, images) => {
    navigation.style.visibility = "hidden";
    imagesPreviewContainer.style.top = `${scroll}px`;
    imagesPreviewContainer.style.display = "flex";

    const imgElement = document.createElement("img");
    imgElement.src = images[0];
    imgElement.classList.add("showImage");
    imagesPreview.appendChild(imgElement);

    
    document.getElementsByTagName("body")[0].style.overflow = "hidden";

    imagesForDisplay = images;

    updateImagesPreviewSlider(images);
}

projectsTypeGalery_modeling_1.addEventListener("click", () => {
    displayImagePrewiew(document.documentElement.scrollTop, projectsTypeGalery_modeling_1_images);
});

projectsTypeGalery_enterijer_1.addEventListener("click", () => {
    displayImagePrewiew(document.documentElement.scrollTop, projectsTypeGalery_enterijer_1_images);
});

projectsTypeGalery_exterier_1.addEventListener("click", () => {
    displayImagePrewiew(document.documentElement.scrollTop, projectsTypeGalery_exterier_1_images);
});

const closeImagePreview = () => {
    navigation.style.visibility = "visible";
    imagesPreviewContainer.style.display = "none";
    document.getElementsByTagName("body")[0].style.overflow = "scroll";
    imagesPreview.removeChild(imagesPreview.childNodes[1]);
    index = 0;
};

closePreview.addEventListener("click", closeImagePreview);



const imagesSlider = document.getElementById("imagesSlider");

const updateImagesPreviewSlider = (images) => {
    imagesSlider.innerText = null;
    for(let i = 0; i < images.length; i++){
        const buttonElement = document.createElement("button");
        buttonElement.addEventListener("click", () => {
            showNextImage(i);
            index = i;
        })
        if(i === 0){
            buttonElement.style.transform = "scale(1.5)";
        };
        imagesSlider.appendChild(buttonElement);
    };
};

const handleImagesPreviewSliderScale = (index, images) => {
    for(let i = 0; i < images.length; i++) {
        const buttonElement = imagesSlider.getElementsByTagName("button")[i];
        if( i === index){
            buttonElement.style.transform = "scale(1.5)";
        }
        else{
            buttonElement.style.transform = "scale(1)";
        };
    };
};

const navigationElement2 = document.getElementById("navigationElement2");
const navigationElement3 = document.getElementById("navigationElement3");
const navigationElement4 = document.getElementById("navigationElement4");


const projectsTypeDescription_2 = document.getElementById("projectsTypeDescription_2");
const projectsTypeDescription_3 = document.getElementById("projectsTypeDescription_3");

const projectsTypeDescription_2_position = projectsTypeDescription_2.offsetTop - 50;
const projectsTypeDescription_3_position = projectsTypeDescription_3.offsetTop - 50;


window.addEventListener("scroll", () => {
    let scroll = document.documentElement.scrollTop;
    // handleNavigationScrollStyleEfect(scroll)
    if(window.innerWidth < 500){
        handleNavigationScrollContentFlowEffect(scroll, projectsTypeDescription_2_position, projectsTypeDescription_3_position)
    };
});

const handleNavigationScrollContentFlowEffect = (scroll, firstChange, secondChange) => {
    if(scroll < firstChange){
        navigationStyle(navigationElement2, 1.2 , "bold");
        navigationStyle(navigationElement3, 1.0 , "normal");
        navigationStyle(navigationElement4, 1.0 , "normal");
    }
    else if(scroll >= firstChange && scroll < secondChange){
        navigationStyle(navigationElement2, 1.0 , "normal");
        navigationStyle(navigationElement3, 1.2 , "bold");
        navigationStyle(navigationElement4, 1.0 , "normal");
    }
    else{
        navigationStyle(navigationElement2, 1.0 , "normal");
        navigationStyle(navigationElement3, 1.0 , "normal");
        navigationStyle(navigationElement4, 1.2 , "bold");
    };
};

window.addEventListener("load", () => {
    if(window.innerWidth < 500){
    navigationStyle(navigationElement2, 1.2 , "bold");
    };
});

const navigationStyle = ( element, scale, font) => {
    element.style.transform = `scale(${scale})`;
    element.style.fontWeight = font;
};