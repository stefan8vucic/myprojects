const form = document.getElementById("form");
const contactData = document.getElementById("contactData");
const showMail = document.getElementById("showMail");

showMail.addEventListener("click", () => {
    form.style.height = "300px";
    form.style.paddingLeft = "10px";
    showMail.style.opacity = 0;
    showMail.style.height = "0px";

    setTimeout(() => {
        contactsbotom += 245;

        findDOMElementsPosition(document.documentElement.scrollTop)
    }, 300);
});

let navigaotionBackgroundChange = window.innerWidth * 0.05;

window.addEventListener("resize", () => {
    navigaotionBackgroundChange = window.innerWidth * 0.05;
});


const navigation = document.getElementById("navigation");
const container = document.getElementById("container");
const aaa = document.getElementById("aaa");
const about = document.getElementById("about");
const projekti = document.getElementById("projekti");
const contact = document.getElementById("contact");


const containerHight = container.offsetHeight;
const visinaAbout = aaa.getBoundingClientRect().top;
const projectsOffset = (projekti.getBoundingClientRect().top - 60);

let contactOffset = contact.getBoundingClientRect().top;

let contactsbotom = contact.getBoundingClientRect().bottom;

const navigationElement1 = document.getElementById("navigationElement1");
const navigationElement2 = document.getElementById("navigationElement2");
const navigationElement3 = document.getElementById("navigationElement3");
const navigationElement4 = document.getElementById("navigationElement4");
const www = document.getElementById("www");

window.addEventListener("scroll", () => {
    let scroll = document.documentElement.scrollTop;
    findDOMElementsPosition(scroll);
});


const findDOMElementsPosition = (scroll) => {
    const headerHeight = container.offsetHeight;
    const beforeElementHeight = window.innerWidth * 0.05;
    const navigationBackgroundChange = headerHeight - (beforeElementHeight + 108);
    let contactsChange;

    if(contactsbotom - contactOffset > window.innerHeight){
        contactsChange = contactOffset;
    }
    else{
        contactsChange = contactsbotom - (window.innerHeight + 20);
    };
    
    handleNavigationScrollStyleEfect(scroll, navigationBackgroundChange);
    if(window.innerWidth < 500){
        handleNavigationScrollContentFlowEffect(scroll, navigationBackgroundChange, projectsOffset, contactsChange);
    };
};


const navigationStyle = ( element, scale, font) => {
    element.style.transform = `scale(${scale})`;
    element.style.fontWeight = font;
};


const handleNavigationScrollContentFlowEffect = (scroll, firstChange, secondChange, thirdChange) => {
    if(scroll < 40){
        navigationStyle(navigationElement1, 1.0 , "normal");
        navigationStyle(navigationElement2, 1.0 , "normal");
        navigationStyle(navigationElement3, 1.0 , "normal");
        navigationStyle(navigationElement4, 1.0 , "normal");
    }
    else if(scroll >= 40 && scroll < firstChange){
        navigationStyle(navigationElement1, 1.2 , "bold");
        navigationStyle(navigationElement2, 1.0 , "normal");
        navigationStyle(navigationElement3, 1.0 , "normal");
        navigationStyle(navigationElement4, 1.0 , "normal");
    }
    else if(scroll >= firstChange && scroll < secondChange){
        navigationStyle(navigationElement1, 1.0 , "normal");
        navigationStyle(navigationElement2, 1.2 , "bold");
        navigationStyle(navigationElement3, 1.0 , "normal");
        navigationStyle(navigationElement4, 1.0 , "normal");
    }
    else if(scroll >= secondChange && scroll < thirdChange){
        navigationStyle(navigationElement1, 1.0 , "normal");
        navigationStyle(navigationElement2, 1.0 , "normal");
        navigationStyle(navigationElement3, 1.2 , "bold");
        navigationStyle(navigationElement4, 1.0 , "normal");
    }
    else{
        navigationStyle(navigationElement1, 1.0 , "normal");
        navigationStyle(navigationElement2, 1.0 , "normal");
        navigationStyle(navigationElement3, 1.0 , "normal");
        navigationStyle(navigationElement4, 1.2 , "bold");
    };
};


const handleNavigationScrollStyleEfect = (scroll, change) => {
    if(scroll < 30){
        navigation.style.backgroundColor = "transparent";
        navigation.style.transform = "scale(1.0)";
    }
    else if(scroll >= 30 && scroll < change) {
        navigation.style.backgroundColor = "rgba(255, 255, 255, 0.2)";
        navigation.style.transform = "scale(0.8)";
        
    }
    else {
        navigation.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
        navigation.style.transform = "scale(0.8)";
    };
};