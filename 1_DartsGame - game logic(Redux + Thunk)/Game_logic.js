const game_mode = {
    game_type: "", // Online & Offline
    game_mode: "", // 301 & 501  
    game_option: "", // SingleAll, DoubleIn, DoubleOut, DoubleAll
    game_end: "" // Egal, WhoFirst
};

// when game_type === Online, players will compete for ranking_points for global ranking system
// ranking points are calculated during the game for high-points score and when game is finished 
//for players achieved score and select game modes - calculation is represented on the end of the file 

const data = {
    // player for points 
    active_player: 0,
    turn_counter: 1,
    throw_counter: 0,
    //player score by throw
    turn_score: [0, 0, 0],
    // points for ranking by turn
    turn_rank_points: 0,
    // in Egal gameMode, if turn is not finished and player score === 0, egal_winner = true
    egal_winner: false,
    // in any combination of same score and throws when game is finished, playOff = true
    playOff: false,
    playOff_players: [],
    // players in game
    players: []
};

// after every points update this function will be dispatched from component with points value and score code
//code: 0-20 - points: 0-20
//code: 21-40 - points: 2 * 1-20
//code: 41-60 - points: 3 * 1-20
//code: 81 - points: 25
//code: 82 - points: 50

export const Handle_scoreUpdate = (points, code) => {
    return (dispatch) => {
        if (game_mode.game_mode === 501 && data.turn_counter > 15) {
            dispatch(Check_players_for_playOff(data.players));
        }
        else if (game_mode.game_mode === 301 && data.turn_counter > 10) {
            dispatch(Check_players_for_playOff(data.players));
        }
        else {
            if (data.throw_counter < 3) {
                if (game_mode.game_option === "DoubleIn" || game_mode.game_option === "DoubleAll") {
                    if (data.players[data.active_player].score === game_mode.game_type) {
                        if (code < 21) {
                            data.turn_score[data.throw_counter] = 0;
                            data.throw_counter += 1;
                            dispatch(Sort_players(data));
                        }
                        else {
                            dispatch(Update_score(data, game_mode, points, code));
                        };
                    }
                    else {
                        dispatch(Update_score(data, game_mode, points, code));
                    };
                }
                else {
                    dispatch(Update_score(data, game_mode, points, code));
                };
            }
            else {
                return;
            };
        };
    };
};

//after initial check, throw score will be updated
export const Update_score = (data, game_mode, points, code) => {
    return (dispatch) => {
        data.players[data.active_player].score -= points;
        data.players[data.active_player].throws += 1;
        data.turn_score[data.throw_counter] = points;
        data.throw_counter += 1;
        // if game_mode is Online, player will get rank points for certain score
        if (game_mode.game_mode === "Online") {
            if (points === 60 || points === 57 || points === 50) {
                data.players[data.active_player].points.game_throws += 0.2;
                data.turn_rank_points += 0.2;
            }
            else if (points === 54 || points === 51 || points === 48) {
                data.players[data.active_player].points.game_throws += 0.1;
                data.turn_rank_points += 0.1;
            };
        };
        dispatch(Game_over_check(data, game_mode, code));
    };
};

// this function's responsibility is the finish of the game
export const Game_over_check = (data, game_mode, code) => {
    return (dispatch) => {
        // in case of egal game check_turn will check if certain game turn is finished
        const check_turn = data.players.every(player => player.turn === data.players[data.active_player].turn);
        if (data.players[data.active_player].score > 0) {
            if ((game_mode.game_option === "DoubleOut" || game_mode.game_option === "DoubleAll") && data.players[data.active_player].score === 1) {
                dispatch(Handle_invalid_score(data, check_turn, game_mode));
            }
            else {
                if (game_mode.game_end === "Egal") {
                    if (data.throw_counter === 3) {
                        dispatch(Handle_egal_game(data, check_turn, false));
                    }
                    else {
                        dispatch(Sort_players(data));
                    };
                }
                else {
                    dispatch(Sort_players(data));
                };
            };
        }
        else if (data.players[data.active_player].score === 0) {
            if (game_mode.game_option === "DoubleOut" || game_mode.game_option === "DoubleAll") {
                if ((code > 20 && code < 61) || code === 82) {
                    if (data.players.length > 1) {
                        if (game_mode.game_end === "Egal") {
                            dispatch(Handle_egal_game(data, check_turn, true));
                        }
                        else {
                            setTimeout(() => {
                                dispatch(Check_players_for_playOff(data.players));
                            }, 500);
                        };
                    }
                    else {
                        dispatch(Last_game_check(data.players));
                    };
                }
                else {
                    dispatch(Handle_invalid_score(data, check_turn, game_mode));
                };
            }
            else {
                if (data.players.length > 1) {
                    if (game_mode.game_end === "Egal") {
                        dispatch(Handle_egal_game(data, check_turn, true));
                    }
                    else {
                        setTimeout(() => {
                            dispatch(Check_players_for_playOff(data.players));
                        }, 500);
                    };
                }
                else {
                    dispatch(Last_game_check(data.players));
                };
            };
        }
        else {
            dispatch(Handle_invalid_score(data, check_turn, game_mode));
        };
    };
};

// this function handles the situation if score is < 0, and in DoubleOut or DoubleAll mode if score is 0 with code < 21 or if sore === 1;
export const Handle_invalid_score = (data, check_turn, game_mode) => {
    return (dispatch) => {
        more_points_audio.play();
        let score_sum = data.turn_score.reduce(function (a, b) {
            return a + b;
        }, 0);
        data.players[data.active_player].throws -= data.throw_counter;
        data.players[data.active_player].score += score_sum;
        data.throw_counter = 3;
        // ranking points are reset for the turn where invalid score happen
        if (game_mode.game_mode === "Online") {
            data.players[data.active_player].points.game_throws -= data.turn_rank_points;
        };

        if (game_mode.game_end === "Egal") {
            dispatch(Handle_egal_game(data, check_turn, false));
        }
        else {
            dispatch(Sort_players(data));
        };
    };
};

// in case of Egal game, this function check if turn has finished and handels if there is a egal winner
export const Handle_egal_game = (data, check_turn, zero_points) => {
    return (dispatch) => {
        // if current player score is === 0
        if (zero_points) {
            if (check_turn) {
                dispatch(Sort_players(data));
                setTimeout(() => {
                    dispatch(Check_players_for_playOff(data.players));
                }, 500);
            }
            else {
                if (data.throw_counter !== 3) {
                    for (let i = data.throw_counter; i < 3; i++) {
                        data.turn_score[i] = 0;
                    };
                    data.throw_counter = 3;
                };
                dispatch(Update_egal_winner(true));
                dispatch(Sort_players(data));
            };
        }
        else {
            if (check_turn) {
                if (!data.egal_winner) {
                    dispatch(Sort_players(data));
                }
                else {
                    setTimeout(() => {
                        dispatch(Check_players_for_playOff(data.players));
                    }, 500);
                };
            }
            else {
                dispatch(Sort_players(data));
            };
        };
    };
};

// when game is finished all players with same score will be proceeded to playoff
// player with same score but less throws will automatically get higher rank
export const Check_players_for_playOff = (players) => {
    return (dispatch) => {
        // sorting players by score and throws
        players.sort((a, b) => {
            if (a.score > b.score) {
                return 1;
            }
            else if (b.score > a.score) {
                return -1;
            }
            else {
                return a.throws - b.throws;
            };
        });

        // updating new rank, with same rank value for same score
        let rank_counter = 1;
        let rank_counter_handler = 1;
        for (let i = 0; i < players.length; i++) {
            players[i].rank = rank_counter;
            if (players[i + 1]) {
                if ((players[i].score === players[i + 1].score) && (players[i].throws === players[i + 1].throws)) {
                    rank_counter_handler++;
                }
                else {
                    rank_counter += rank_counter_handler;
                    rank_counter_handler = 1;
                };
            };
        };
        // extracting arrays of players for play off if rank is same
        let playOff_players = [];
        let prevent_duplication_handler = [];

        players.forEach(player_check => {
            let players_for_playOff = players.filter(player => player.rank === player_check.rank);
            if (players_for_playOff.length > 1) {
                // preventing the same combination of players
                if (!prevent_duplication_handler.some(rank => rank === player_check.rank)) {
                    playOff_players = [...playOff_players, players_for_playOff];
                    prevent_duplication_handler = [...prevent_duplication_handler, players_for_playOff[0].rank]
                };
            };
        });

        if (playOff_players.length > 0) {
            setTimeout(() => {
                dispatch(Update_playOff(true, playOff_players));
            }, 500);
        }
        else {
            dispatch(Last_game_check(players));
        };
    };
};

// after players with same rank have played, this function will be dispatched from PlayOff component 
// when dispatched, it will check the results and update the new rank, in case of the same result players will play again 
export const Update_players_from_playOff_thunk = (players) => {
    return (dispatch, getState) => {
        
        let playOff_players_state = [...getState().Game_playground.playOff_players];
        players.sort((a, b) => b.playOff_score - a.playOff_score);

        // updateing new rank
        let rank_counter = players[0].rank;
        let rank_counter_handler = 1;
        for (let i = 0; i < players.length; i++) {
            players[i].rank = rank_counter;
            if (players[i + 1]) {
                if (players[i].playOff_score === players[i + 1].playOff_score) {
                    rank_counter_handler++;
                }
                else {
                    rank_counter += rank_counter_handler
                    rank_counter_handler = 1;
                };
            };
        };

        // extracting players for play off
        let playOff_players = [];
        let playOff_players_for_update = [];
        let prevent_duplication_handler = [];

        players.forEach(player_check => {
            let players_for_playOff = players.filter(player => player.rank === player_check.rank);
            if (players_for_playOff.length > 1) {
                if (!prevent_duplication_handler.some(rank => rank === player_check.rank)) {
                    players_for_playOff.forEach(player => player.playOff_score = 0);
                    playOff_players = [...playOff_players, players_for_playOff];
                    prevent_duplication_handler = [...prevent_duplication_handler, players_for_playOff[0].rank]
                };
            }
            else {
                playOff_players_for_update = [...playOff_players_for_update, player_check];
            };
        });

        // in case of tree players in playOff, with two of them with same playOff score, third player will be updated with rank and two players will play again
        if (playOff_players_for_update.length > 0) {
            dispatch(Update_players_from_playOff(playOff_players_for_update));
        };
        // last array of playOff players will be deleted
        playOff_players_state.pop();

        // after turn in playOff is played with same score thos players will play again
        if (playOff_players.length > 0) {
            playOff_players_state = [...playOff_players_state, ...playOff_players];
            dispatch(Update_playOff(true, playOff_players_state));
        }
        // after turn in playOff is played with different score, playOff will continue with next game-equal-score players
        else if (playOff_players_state.length > 0) {
            dispatch(Update_playOff(true, playOff_players_state));
        }
        // if no more players for playOff, the report will be prepared
        else {
            setTimeout(() => {
                dispatch(Last_game_check(players));
            }, 500);
        };
    };
};

// this function handles Online and Offline game mode report 
export const Last_game_check = () => {
    return (dispatch) => {
        dispatch(Set_loading(true));
        if (game_mode.game_mode === "Online") {
            dispatch(Update_players_rank_points());
        }
        else {
            dispatch(Update_game_report(data));
        };
    };
};

// if gameMode === Online, player's rank points will be updated for classement and selected gameOptions 
export const Update_players_rank_points = () => {
    return (dispatch) => {
        data.players.sort((a, b) => a.rank - b.rank);

        dispatch(Update_winner_in_dataBase(data.players[0].player_id));

        data.players[0].points.game += 2;

        if (game_mode.game_type === 501) {
            data.players[0].points.option += 0.5;
        };

        if (game_mode.game_option === "DoubleIn" || game_mode.game_option === "DoubleOut") {
            data.players[0].points.option += 0.3;
        }
        else if (game_mode.game_option === "DoubleAll") {
            data.players[0].points.option += 0.4;
        };

        let game_points = 0.5;
        for (let i = data.players.length - 1; i >= 0; i--) {
            data.players[i].points.game += game_points;
            game_points += 0.5;

            if (game_mode.game_option === "DoubleIn" && data.players[i].score < Number(game_mode.game_type)) {
                data.players[i].points.option += 0.1;
            }
            else if (game_mode.game_option === "DoubleOut" && data.players[i].score === 0) {
                data.players[i].points.option += 0.1;
            }
            else if (game_mode.game_option === "DoubleAll") {
                if (data.players[i].score < Number(game_mode.game_type)) {
                    data.players[i].points.option += 0.1;
                }
                else if (data.players[i].score === 0) {
                    data.players[i].points.option += 0.2;
                };
            };
        };

        for (let i = 0; i < data.players.length; i++) {
            const { game, game_throws, option } = data.players[i].points;
            data.players[i].points.total = game + game_throws + option;
        };
        // Update_game_report updates results in database and presents game results in DOM
        dispatch(Update_game_report(data.players));
    };
};

export default Game_playground;