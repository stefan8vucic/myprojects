import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import WebShopLogo from '../Photos/logo.png';
import CartImage from '../Photos/cart.png';
import AccountImage from '../Photos/avatar.png';

function Header() {

    const cartProductsQuantity = useSelector(state => state.Cart.cartProductsQuantity);
    const isLogged = useSelector(state => state.GetSession.isLogged);
    const sessionID = useSelector(state => state.GetSession.user.id);
    const sessionUserName = useSelector(state => state.GetSession.user.userName);

    return (
        <div className="headerContainer">
            <div className="webShopLogoContainer">
                <Link to="/" >
                    <img src={WebShopLogo} alt="WebShopLogo"></img>
                </Link>
            </div>
            <div className="headerNavigationContainer">
                <Link to="/">
                    <p>HOME</p>
                </Link>
                <Link to="/store">
                    <p>STORE</p>
                </Link>
                <Link to="/contact">
                    <p>CONTACT</p>
                </Link>
            </div>
            <div className="headerIconsContainer">
                <Link to={isLogged ? `/${sessionID}/webshop-history` : "/log-in"}>
                    <div className="accountImageContainer">
                        <img src={AccountImage} alt="AccountImage"></img>
                        {isLogged ? sessionUserName : "LogIn"}
                    </div>
                </Link>
                <Link to="/cart">
                    <div className="cartImageContainer">
                        <span id="numberOfProducts">{cartProductsQuantity}</span>
                        <img src={CartImage} alt="CartImage"></img>
                    </div>
                </Link>
            </div>
        </div>
    );
};

export default Header;