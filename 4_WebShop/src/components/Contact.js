import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PhoneImage from '../Photos/phone.png';
import TimeImage from '../Photos/clock.png';
import MailImage from '../Photos/mail.png';

function Contact() {

    const user = useSelector(state => state.GetSession.user);
    const { userName, email } = user;

    const [emailFormDisplay, setEmailFormDisplay] = useState("none");
    const [sentEmailConfirmationClass, setSentEmailConfirmationClass] = useState("");

    const [newEmailTemplate, setNewEmailTemplate] = useState({
        name: userName ? userName : "",
        email: email ? email : "",
        subject: "",
        content: ""
    });

    const handleNewEmailInput = (e) => {
        setNewEmailTemplate({
            ...newEmailTemplate,
            [e.target.name]: e.target.value
        });
    };

    const sendEmail = (e) => {
        e.preventDefault();
        setSentEmailConfirmationClass("sentEmailConfirmationContainer");
        setEmailFormDisplay("none");
        setNewEmailTemplate({
            ...newEmailTemplate,
            subject: "",
            content: ""
        })
        setTimeout(() => {
            setSentEmailConfirmationClass("");
        }, 5000);
    }

    return (
        <div className="contactContainer">
            <div className={sentEmailConfirmationClass} style={{ opacity: 0, height: "50px" }}>
                Your message was sent successfully!
            </div>
            <div className="contactGeneralInfo">
                <div>
                    <img src={PhoneImage} alt="phone"></img> 0033642792236
                </div>
                <div>
                    <img src={TimeImage} alt="time"></img> Monday - Friday <br /> 09:00 - 18:00h
                </div>
                <div>
                    <img src={MailImage} alt="E-mail"></img> contact@web-shop.shop <br />
                </div>
            </div>
            <button onClick={() => { emailFormDisplay === "none" ? setEmailFormDisplay("flex") : setEmailFormDisplay("none") }}>
                {emailFormDisplay === "none" ? "CONTACT US" : "HIDE FORM"}
            </button>
            <div className="contactEmailFormContainer" style={{ display: emailFormDisplay }}>
                <form onSubmit={sendEmail}>
                    <input type="text" onChange={handleNewEmailInput} name="name"
                        defaultValue={userName} autoComplete="off" required placeholder="Name"></input><br />
                    <input type="text" onChange={handleNewEmailInput} name="email"
                        defaultValue={email} autoComplete="off" required placeholder="E-mail"></input><br />
                    <input type="text" onChange={handleNewEmailInput} name="subject" value={newEmailTemplate.subject}
                        autoComplete="off" required placeholder="Subject"></input><br />
                    <textarea onChange={handleNewEmailInput} name="content" value={newEmailTemplate.content}
                        autoComplete="off" required placeholder="Your question..."></textarea><br />
                    <button type="submit">SEND</button>
                </form>
            </div>
            <h2>- Locations of our shops -</h2>
            <div className="contactGeoLocation">
                <iframe src="https://www.google.com/maps/d/u/0/embed?mid=187t4quDnx-fVZ7m2gwHR9sk3yLSsfECL" width="640" height="480" title="WebShopLocations"></iframe>
            </div>
        </div>
    );
};

export default Contact;
