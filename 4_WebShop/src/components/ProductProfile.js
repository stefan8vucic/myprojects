import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AddToCart } from './Actions/index.js';
import DisplayImage from '../Photos/display.png';
import CameraImage from '../Photos/camera.png';
import MemoryImage from '../Photos/memory.png';
import RamImage from '../Photos/ram.png';

function ProductProfile({ match }) {

    const dispatch = useDispatch();

    const allProducts = useSelector(state => state.Products);

    const product = (allProducts.filter(product => product.path === match.params.path))[0];

    const addProductToCart = () => {
        // creating new product object to prevent mutations in source object
        let productToChart = Object.assign({ ...product });
        productToChart.quantity = 1;
        dispatch(AddToCart(productToChart));
    };

    const { status, brand, model, currentPrice, oldPrice, discount, image, details: { display, frontCamera, rareCamera, ram, memory, weight, battery } } = product;

    return (
        <div className="productProfileContainer">
            <div className="productProfileOwerview">
                <div className="productProfileOwerviewImage">
                    {status ?
                        <div className="productCardStatus">
                            <p>NEW</p>
                        </div> :
                        oldPrice ?
                            <div className="productCardStatus">
                                <p>{discount}</p>
                            </div> : ""}
                    <img src={image} alt={`${brand}${model}`}></img>
                </div>
                <div className="productProfileOwerviewInfo">
                    <h1>{brand}</h1><br />
                    <h3>{model}</h3>
                    <div className="productProfileOwerviewInfoGraphicContainer">
                        <div className="productProfileOwerviewInfoGraphic">
                            <img src={DisplayImage} alt="Display"></img> <p>{display}</p>
                        </div>
                        <div className="productProfileOwerviewInfoGraphic">
                            <img src={CameraImage} alt="Camera"></img> <p>{frontCamera}</p>
                        </div>
                        <div className="productProfileOwerviewInfoGraphic">
                            <img src={RamImage} alt="Ram"></img> <p>{ram}</p>
                        </div>
                        <div className="productProfileOwerviewInfoGraphic">
                            <img src={MemoryImage} alt="Memory"></img> <p>{memory}</p>
                        </div>
                    </div>
                    <div className="productProfileOwerviewPrice">
                        <h2>{`${currentPrice}€`}</h2>
                        <div className="productProfileOwerviewOldPrice">
                            {oldPrice ? <div className="crossedPrice"></div> : ""}
                            {oldPrice ? <h2>{`${oldPrice}€`}</h2> : ""}
                        </div>
                    </div>
                </div>
            </div>
            <div className="addToCartButton">
                <button onClick={addProductToCart}>ADD TO CART</button>
            </div>
            <br /><br /><br />
            <div className="productProfileDetails">
                Brand: {brand} <br />
                Model: {model} <br />
                Display: {display} <br />
                Front Camera: {frontCamera} <br />
                Selfie Camera: {rareCamera} <br />
                <strong>Memory:</strong>  <br />
                Ram: {ram}<br />
                Storage: {memory} <br />
                Battery: {battery} <br />
                Weight: {weight} <br />
            </div>
        </div>
    );
};

export default ProductProfile;