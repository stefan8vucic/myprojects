import { combineReducers } from 'redux';
import Products from './AllProducts.js';
import Cart from './Cart.js';
import RegisteredUsers from './RegisteredUsers.js';
import GetSession from './GetSession.js';

const allReducers = combineReducers({
    Products,
    Cart,
    RegisteredUsers,
    GetSession
});

export default allReducers;