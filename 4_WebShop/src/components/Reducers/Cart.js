import { ADD_TO_CART, EMPTY_CHART, REDUCE_CART_QUANTITY, REMOVE_PRODUCT_FROM_CART } from "../Actions/type";

const initialState = {
    cartProducts: [],
    cartProductsQuantity: 0,
    cartProductsTotalPrice: 0
};

const Cart = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART: {

            // by adding the product to cart, it checks if that exact product is already added, if yes, it increases its quantity otherwise it will add that new product with quantity = 1
            if (state.cartProducts.length > 0) {
                let findProduct = state.cartProducts.find(product => product.id === action.payload.id)

                if (findProduct) {
                    state.cartProducts.filter(product => product.id === findProduct.id)[0].quantity++;

                    return {
                        ...state,
                        cartProductsQuantity: state.cartProductsQuantity + 1,
                        cartProductsTotalPrice: state.cartProductsTotalPrice + action.payload.currentPrice
                    }
                }
                else {
                    return {
                        ...state,
                        cartProducts: state.cartProducts.concat(action.payload),
                        cartProductsQuantity: state.cartProductsQuantity + 1,
                        cartProductsTotalPrice: state.cartProductsTotalPrice + action.payload.currentPrice
                    }
                }
            }
            else {
                return {
                    ...state,
                    cartProducts: state.cartProducts.concat(action.payload),
                    cartProductsQuantity: state.cartProductsQuantity + 1,
                    cartProductsTotalPrice: state.cartProductsTotalPrice + action.payload.currentPrice
                }
            }
        }

        // reducing the quantity of particular product, if quantity = 0, it will be deleted
        case REDUCE_CART_QUANTITY: {
            state.cartProducts.filter(product => product.id === action.payload.id)[0].quantity--
            return {
                ...state,
                cartProductsQuantity: state.cartProductsQuantity - 1,
                cartProductsTotalPrice: state.cartProductsTotalPrice - action.payload.currentPrice
            }
        }

        case REMOVE_PRODUCT_FROM_CART: {
            state.cartProducts.splice(state.cartProducts.findIndex(product => product.id === action.payload.id), 1);
            return {
                ...state,
                cartProducts: state.cartProducts,
                cartProductsQuantity: state.cartProductsQuantity - action.payload.quantity,
                cartProductsTotalPrice: state.cartProductsTotalPrice - (action.payload.quantity * action.payload.currentPrice)
            }
        }

        case EMPTY_CHART:
            return {
                ...state,
                cartProducts: [],
                cartProductsQuantity: 0,
                cartProductsTotalPrice: 0
            };

        default:
            return state
    };
};

export default Cart;