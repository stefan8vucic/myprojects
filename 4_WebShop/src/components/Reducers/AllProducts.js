import { products } from '../appData.js';

const initialState = products;

const Products = (state = initialState, action) => {
    switch (action.type) {

        default:
            return state;
    }
}

export default Products;