import * as Action from '../Actions/type';

const initialState = {
    isLogged: false,
    user: {}
};

const GetSession = (state = initialState, action) => {
    switch (action.type) {
        case Action.GET_SESSION: {
            action.payload.isLogged = true;
            return {
                ...state,
                isLogged: true,
                user: action.payload
            };
        }

        case Action.LOG_OUT: {
            state.user.isLogged = false;
            return {
                ...state,
                isLogged: false,
                user: []
            };
        }

        case Action.CREATE_PAYMENT_METHOD: {
            return {
                ...state,
                paymentMethodTemplate: {
                    index: "",
                    cardType: "",
                    cardNumber: "",
                    expiryDate: "",
                    cardHolderName: ""
                }
            };
        }

        case Action.MODIFY_PAYMENT_METHOD: {
            return {
                ...state,
                paymentMethodTemplate: action.payload
            };
        }

        case Action.UPLOAD_NEW_PAYMENT_METHOD: {
            let { user } = state;
            action.payload.index = user.paymentMethods.length;
            user.paymentMethods.push(action.payload);

            return {
                ...state,
                user: user
            };
        };

        case Action.UPLOAD_MODIFIED_PAYMENT_METHOD: {
            let { user } = state;
            user.paymentMethods.splice(action.payload.index, 1, action.payload);

            return {
                ...state,
                user: user
            };
        };

        case Action.DELETE_PAYMENT_METHOD: {
            let { user } = state;
            user.paymentMethods.splice(action.payload, 1);
            // after one of the methods is deleted it will reorder rest of the methods
            for (let i = 0; i < user.paymentMethods.length; i++) {
                user.paymentMethods[i].index = i
            }

            return {
                ...state,
                user: user
            };
        };

        case Action.CREATE_DELIVERY_INFORMATIONS: {
            return {
                ...state,
                deliveryInformationsTemplate: {
                    index: "",
                    firstName: "",
                    lastName: "",
                    address: "",
                    building: "",
                    app: "",
                    zipCode: "",
                    city: "",
                    country: "",
                    telNo: ""
                }
            };
        }

        case Action.MODIFY_DELIVERY_INFORMATIONS: {
            return {
                ...state,
                deliveryInformationsTemplate: action.payload
            };
        }

        case Action.UPLOAD_NEW_DELIVERY_INFORMATIONS: {
            let { user } = state;
            action.payload.index = user.deliveryInformations.length;
            user.deliveryInformations.push(action.payload)

            return {
                ...state,
                user: user
            };
        };

        case Action.UPLOAD_MODIFIED_DELIVERY_INFORMATIONS: {
            let { user } = state;
            user.deliveryInformations.splice(action.payload.index, 1, action.payload);

            return {
                ...state,
                user: user
            };
        };

        case Action.DELETE_DELIVERY_INFORMATIONS: {
            let { user } = state;
            user.deliveryInformations.splice(action.payload, 1);
            // after one of the deliveryInformations is deleted it will reorder rest of the deliveryInformations 
            for (let i = 0; i < user.deliveryInformations.length; i++) {
                user.deliveryInformations[i].index = i
            }

            return {
                ...state,
                user: user
            };
        };

        case Action.UPLOAD_NEW_ORDER: {
            let { user } = state;
            user.shoppingHistory.push(action.payload);
            
            return {
                ...state,
                user: user
            };
        };

        default:
            return state;
    };
};

export default GetSession;