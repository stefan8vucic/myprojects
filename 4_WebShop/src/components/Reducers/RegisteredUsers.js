import { REGISTER_NEW_ACCOUNT } from '../Actions/type';
import { users } from '../appData.js';

const initialState = {
    nextId: 1000002,
    registeredUsers: users
};

const RegisteredUsers = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_NEW_ACCOUNT: {
            return {
                ...state,
                registeredUsers: state.registeredUsers.concat(action.payload),
                nextId: state.nextId + 1
            }
        }

        default:
            return state;
    }
};

export default RegisteredUsers;