import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ModifyDeliveryInformations } from './Actions/index';
import DeleteImage from '../Photos/delete.png';
import EditImage from '../Photos/edit.png';

function DeliveryInformationsCard(props) {

    const dispatch = useDispatch();
    const history = useHistory();

    const sessionID = useSelector(state => state.GetSession.user.id);

    const { index, firstName, lastName, address, telNo } = props.deliveryInformations;

    const modifyDeliveryInformations = () => {
        dispatch(ModifyDeliveryInformations(props.deliveryInformations));
        history.push(`/${sessionID}/modify-delivery/${index}`);
    };

    return (
        <div className="addressesInformationsCardContainer">
            {firstName} {lastName} <br />
            {address} <br />
            {telNo} <br />
            <img onClick={() => props.delete(index)} src={DeleteImage} alt="Delete" title="Delete"></img>
            <img onClick={modifyDeliveryInformations} src={EditImage} alt="Edit" title="Edit"></img>
        </div>
    );
};

export default DeliveryInformationsCard;