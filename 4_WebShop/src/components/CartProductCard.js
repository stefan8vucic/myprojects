import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { AddToCart, ReduceCartQuantity, RemoveProductFromCart } from './Actions/index';
import DeleteImage from '../Photos/delete.png';

function CartProductCard(props) {

    const dispatch = useDispatch();

    const { brand, model, currentPrice, image, quantity } = props.product;

    return (
        <div className="cartProductCardContainer">
            <Link key={props.product.id} to={`/store/${props.product.path}`}>
                <div className="cartProductCardImage">
                    <img src={image} alt="ProductImage"></img>
                </div>
            </Link>
            <div className="cartProductCardDetails">
                <div className="cartProductCardDetailsProduct">
                    <h2>{brand}</h2>
                    <h3>{model}</h3>
                </div>
                <div className="cartProductCardDetailsQuantity">
                    Quantity <br />
                    <button onClick={props.product.quantity > 1 ?
                        () => dispatch(ReduceCartQuantity(props.product)) :
                        () => dispatch(RemoveProductFromCart(props.product))}>-</button>
                    {quantity}
                    <button onClick={() => dispatch(AddToCart(props.product))}>+</button>
                </div>
                <div className="cartProductCardDetailsDelete">
                    <img onClick={() => dispatch(RemoveProductFromCart(props.product))} src={DeleteImage} alt="DeleteImage" title="Delete"></img>
                </div>
                <div className="cartProductCardDetailsPrice">
                    <p>{`${currentPrice}€`}</p>
                </div>
            </div>
        </div>
    );
};

export default CartProductCard;
