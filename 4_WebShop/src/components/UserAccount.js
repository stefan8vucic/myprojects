import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';
import { LogOut } from './Actions/index';
import { Link } from 'react-router-dom';
import DeliveryInformations from './DeliveryInformations';
import PaymentsMethods from './PaymentsMethods';
import ShoppingHistory from './ShoppingHistory';
import PaymentMethodTemplate from './PaymentMethodTemplate';
import DeliveryInformationsTemplate from './DeliveryInformationsTemplate';
import ShoppingHistoryOverview from "./ShoppingHistoryOverview";

function Account({ match }) {

    const dispatch = useDispatch();
    const history = useHistory();

    const logOut = () => {
        dispatch(LogOut());
        history.push('/log-in');
    };

    return (
        <div className="myAccountContainer">
            <div className="myAccountNavigationContainer">
                <Link to={`${match.url}/webshop-history`}>
                    <h2>WebShop history</h2> <br />
                </Link>
                <Link to={`${match.url}/deliveries`}>
                    <h2>Delivery informations</h2> <br />
                </Link>
                <Link to={`${match.url}/payment-methods`}>
                    <h2>Payment methods</h2> <br />
                </Link>
                <button onClick={logOut}>LogOut</button>
            </div>
            <div className="myAccountDisplayContainer">
                <Switch>
                    <Route path="/:userID/webshop-history" component={ShoppingHistory} />
                    <Route path="/:userID/deliveries" component={DeliveryInformations} />
                    <Route path="/:userID/payment-methods" component={PaymentsMethods} />
                    <Route path="/:userID/modify-method/:index" component={PaymentMethodTemplate} />
                    <Route path="/:userID/create-new-method" component={PaymentMethodTemplate} />
                    <Route path="/:userID/modify-delivery/:index" component={DeliveryInformationsTemplate} />
                    <Route path="/:userID/create-new-delivery" component={DeliveryInformationsTemplate} />
                    <Route path="/:userID/order/:orderID" component={ShoppingHistoryOverview} />
                </Switch>
            </div>
        </div>
    );
};

export default Account;
