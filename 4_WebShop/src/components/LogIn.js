import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { GetSession } from './Actions';
import { useHistory, useLocation } from 'react-router-dom';

function LogIn() {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    const registeredUsers = useSelector(state => state.RegisteredUsers.registeredUsers);

    const [userNameErorMessage, setUserNameErorMessage] = useState("");
    const [passwordErorMessage, setPasswordErorMessage] = useState("");

    const [userInput, setUserInput] = useState({
        userName: "",
        password: ""
    });

    const handleUserInput = (e) => {
        const { value, name } = e.target;
        setUserInput({
            ...userInput,
            [name]: value
        });
    };

    const { userName, password } = userInput;

    const logIn = (e) => {
        e.preventDefault();
        if (!userName) {
            setUserNameErorMessage("Please type UserName");
            return;
        }
        else if (!password) {
            setUserNameErorMessage("");
            setPasswordErorMessage("Please type Password");
            return;
        }
        else {
            setPasswordErorMessage("");
        }

        let findUser = registeredUsers.filter(user => user.userName === userName);

        if (findUser.length === 0) {
            setUserNameErorMessage("UserName not found");
            return;
        }
        else if (findUser[0].password !== password) {
            setPasswordErorMessage("Invalid Password");
            setUserNameErorMessage("");
            return;
        }
        else {
            setPasswordErorMessage("");
            setUserInput({
                userName: "",
                password: ""
            });
            dispatch(GetSession(findUser[0]));

            // when rendering logIn component from CheckOut component, location.state = true, 
            if (location.state) {
                history.goBack();
            }
            // in any other case, after user is logged in, user's webshop history will be rendered
            else {
                history.push(`/${findUser[0].id}/webshop-history`);
            };
        };
    };

    return (
        <div className="logIn">
            <form onSubmit={logIn}>
                <input type="text" placeholder="UserName : Stefan" onChange={handleUserInput} name="userName" value={userName}></input><br />
                <p style={{ color: "red", margin: "0px" }}>{userNameErorMessage}</p><br />
                <input type="password" placeholder="Password : 1" onChange={handleUserInput} name="password" value={password}></input> <br />
                <p style={{ color: "red", margin: "0px" }}>{passwordErorMessage}</p><br /><br />
                <button type="submit">LogIn</button><br /><br />
            </form><br /><br />
            <p>First time shoping at WebShop?</p>
            <button onClick={() => history.push("/create-new-account")}>Create New Account</button><br /><br />
        </div>
    );
};

export default LogIn;
