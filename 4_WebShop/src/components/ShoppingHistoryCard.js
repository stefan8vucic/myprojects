import React from 'react';

function ShoppingHistoryCard(props) {

    const { id, totalPrice, time } = props.shoppingHistory;

    return (
        <div className="shoppingHistoryCardContainer">
            <div className="orderNumberContainer">
                {`Order No: ${id}`}
            </div>
            <div className="orderDateContainer">
                {time.toDateString()}<br />
                {time.getHours()}:
                {time.getMinutes() < 10 ? "0" + time.getMinutes() : time.getMinutes()}
            </div>
            <div className="orderTotalContainer">
                <h3>{`${totalPrice}€`}</h3>
            </div>
        </div>
    );
};

export default ShoppingHistoryCard;