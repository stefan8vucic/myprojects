import React from 'react';

function ProductCard(props) {

    const { status, oldPrice, discount, image, name, brand, model, currentPrice } = props.product;

    return (
        <div className="productCard">
            {status ?
                <div className="productCardStatus">
                    <p>NEW</p>
                </div> :
                oldPrice ?
                    <div className="productCardStatus">
                        <p>{discount}</p>
                    </div> : ""}
            <div className="productCardImage">
                <img src={image} alt={name}></img>
            </div>
            <div className="productCardInfo">
                <div className="productCardInfoName">
                    <p>{brand}</p>
                    <p>{model}</p>
                </div>
                <div className="productCardInfoPrice">
                    <div className="productCardInfoCurrentPrice">
                        {`${currentPrice}€`}
                    </div>
                    <div className="productOldPrice">
                        {oldPrice ? <div className="crossedPrice"></div> : ""}
                        {oldPrice ? `${oldPrice}€` : ""}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductCard;
