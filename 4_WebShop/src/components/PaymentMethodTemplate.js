import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { UploadModifiedPaymentMetod, UploadNewPaymentMetod } from './Actions/index'

function PaymentMethodTemplate() {

    const history = useHistory();
    const dispatch = useDispatch();

    const paymentMethodTemplate = useSelector(state => state.GetSession.paymentMethodTemplate)

    const [errorMessage, setErrorMessage] = useState("")
    const [newPaymentMethod, setNewPaymentMethod] = useState({ ...paymentMethodTemplate })

    // dynamic update of newPaymentMethod object
    const handleNewPaymentMethodInput = (e) => {
        if (e.target.name === "expiryDate") {
            handleExpiryDateInput(e)
        }
        else {
            setNewPaymentMethod({
                ...newPaymentMethod,
                [e.target.name]: e.target.value
            });
        };
    };

    // for automatically adding slash between month and year
    // this function separates onKeyPress(to update state only with numbers) and KeyDown(to get "Backspace" user input) events
    const handleExpiryDateInput = (e) => {
        // handling onKeyPress event
        if (e.charCode) {
            // limiting input length to 5
            if (newPaymentMethod.expiryDate.length <= 4) {
                // updating state only with numbers 
                if (e.charCode > 47 && e.charCode < 58) {
                    // adding slash when second character is added
                    if (newPaymentMethod.expiryDate.length === 1) {
                        setNewPaymentMethod({
                            ...newPaymentMethod,
                            [e.target.name]: `${newPaymentMethod.expiryDate}${String.fromCharCode(e.charCode)}/`
                        });
                    }
                    else {
                        setNewPaymentMethod({
                            ...newPaymentMethod,
                            [e.target.name]: `${newPaymentMethod.expiryDate}${String.fromCharCode(e.charCode)}`
                        });
                    };
                }
                else {
                    return;
                };
            }
            else {
                return;
            };
        }
        // handling KeyDown event, and updating state with one less character 
        else if (e.key === "Backspace") {
            let expiryDateHandler = newPaymentMethod.expiryDate.slice(0, -1)
            setNewPaymentMethod({
                ...newPaymentMethod,
                [e.target.name]: expiryDateHandler
            });
        }
        else {
            return;
        };
    };

    const uploadNewPaymentMetod = (e) => {
        e.preventDefault();
        // checking if expiry date input is valid
        if (newPaymentMethod.expiryDate.length === 5) {
            newPaymentMethod.creationDate = new Date();
            dispatch(UploadNewPaymentMetod(newPaymentMethod));
            history.goBack();
        }
        else {
            setErrorMessage("Expiry Date format is not valid");
        };
    };

    const uploadModifiedPaymentMetod = (e) => {
        if (newPaymentMethod.expiryDate.length === 5) {
            e.preventDefault();
            dispatch(UploadModifiedPaymentMetod(newPaymentMethod));
            history.goBack();
        }
        else {
            setErrorMessage("Expiry Date format is not valid");
        };
    };

    const { cardType, cardNumber, expiryDate, cardHolderName, creationDate } = newPaymentMethod;

    return (
        <div className="templateContainer">
            <form onSubmit={creationDate ? uploadModifiedPaymentMetod : uploadNewPaymentMetod}>
                <input type="text" onChange={handleNewPaymentMethodInput} name="cardType" value={cardType} placeholder="Card Type" autoComplete="off" required></input> <br />
                <input type="text" onChange={handleNewPaymentMethodInput} name="cardNumber" value={cardNumber} placeholder="Card Number" minLength={14} autoComplete="off" required></input> <br />
                <input type="text" onChange={(e) => console.log(e.target.value)} onKeyDown={handleNewPaymentMethodInput} onKeyPress={handleNewPaymentMethodInput} name="expiryDate" value={expiryDate} placeholder="Expiry Date" autoComplete="off" required></input> <br />
                <input type="text" onChange={handleNewPaymentMethodInput} name="cardHolderName" value={cardHolderName} placeholder="CardHolder Name" autoComplete="off" required></input> <br />
                <button type="submit">{cardType ? "SAVE" : "CREATE"}</button>
            </form>
            <p style={{ color: "red", fontWeight: "bold" }}>{errorMessage}</p>
        </div>
    );
};

export default PaymentMethodTemplate;