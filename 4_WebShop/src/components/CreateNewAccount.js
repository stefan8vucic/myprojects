import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RegisterNewAccount } from './Actions/index';
import CheckImage from '../Photos/check.png';
import ErrorImage from '../Photos/eror.png';
import InfoImage from '../Photos/info.png';
import { validation, statusImageTitleMessages } from "./appData";

function CreateNewAccount() {

    const dispatch = useDispatch();
    const history = useHistory();

    // next id generated in redux state
    const nextId = useSelector(state => state.RegisteredUsers.nextId)
    const registeredUsers = useSelector(state => state.RegisteredUsers.registeredUsers);

    // storing email and password confirmation input
    const [confirmationInputContent, setConfirmationInputContent] = useState({
        email: "",
        password: ""
    });

    const [statusImage, setStatusImage] = useState({
        userName: InfoImage,
        email: InfoImage,
        password: InfoImage,
        emailConfirmation: InfoImage,
        passwordConfirmation: InfoImage
    });

    // the form filling instructions 
    const [statusImageTitle, setStatusImageTitle] = useState({
        userName: statusImageTitleMessages.userName.default,
        email: statusImageTitleMessages.email.default,
        password: statusImageTitleMessages.password.default,
    });

    const [newAccountTemplate, setNewAccountTemplate] = useState({
        id: null,
        isLogged: false,
        userName: "",
        email: "",
        password: "",
        paymentMethods: [],
        deliveryInformations: [],
        shoppingHistory: [],
        registration: null
    });

    // handling userName, email and password input
    const handleUserMainInput = (e) => {
        const { value, name } = e.target;
        //creating new objects to prevent direct state changes otherwise it wouldn't rerender the component when setState is called
        let statusImageHandler = Object.assign({ ...statusImage });
        let statusImageTitleHandler = Object.assign({ ...statusImageTitle });
        // when input is deleted, statusImage and it's title will be changed
        if (value.length === 0) {
            statusImageHandler[name] = InfoImage;
            statusImageTitleHandler[name] = statusImageTitleMessages[name].default
        }
        else {
            // when input matches requested format (userName, email and password) from imported validation object 
            if (validation[name].test(value)) {
                statusImageHandler[name] = CheckImage;
                statusImageTitleHandler[name] = "Ok";
            }
            //while it doesn't
            else {
                statusImageHandler[name] = ErrorImage;
                statusImageTitleHandler[name] = statusImageTitleMessages[name].typingError;
            }
        }
        // after the user changes email or password input, it will handle email and password confirmation image if their length is greater than 0 
        if (name !== "userName") {
            if (confirmationInputContent[name].length > 0) {
                statusImageHandler[`${name}Confirmation`] = value !== confirmationInputContent[name] ? ErrorImage : CheckImage;
            };
        }
        // 
        setNewAccountTemplate({
            ...newAccountTemplate,
            [name]: value.trim(),
        });
        setStatusImage(statusImageHandler);
        setStatusImageTitle(statusImageTitleHandler);
    };
    // handling email and password confirmation input
    const handleUserConfirmationInput = (e) => {
        const { value, name } = e.target;
        let statusImageHandler = Object.assign({ ...statusImage });
        // when input is deleted, statusImage and its title will be changed
        if (value.length === 0) {
            statusImageHandler[`${name}Confirmation`] = InfoImage;
        }
        else {
            if (!newAccountTemplate[name].includes(value)) {
                statusImageHandler[`${name}Confirmation`] = ErrorImage;
            }
            else if (newAccountTemplate[name] === value) {
                statusImageHandler[`${name}Confirmation`] = CheckImage;
            }
            // while main input value includes confirmation input value, the statusImage will be "InfoImage"
            else {
                statusImageHandler[`${name}Confirmation`] = InfoImage;
            };
        };
        setStatusImage(statusImageHandler);
        setConfirmationInputContent({
            ...confirmationInputContent,
            [name]: value.trim()
        });
    };

    const createNewAccount = (e) => {
        e.preventDefault();
        let statusImageHandler = Object.assign({ ...statusImage });
        let statusImageTitleHandler = Object.assign({ ...statusImageTitle });
        const userNameCheck = registeredUsers.some(user => user.userName === newAccountTemplate.userName);
        const emailCheck = registeredUsers.some(user => user.email === newAccountTemplate.email);
        const formValidationCheck = Object.values(statusImageHandler).every(item => item === CheckImage);

        // if user name already exists check
        if (userNameCheck) {
            statusImageHandler.userName = ErrorImage;
            statusImageTitleHandler.userName = statusImageTitleMessages.userName.duplicationError;
        }
        // if email already exists check
        else if (emailCheck) {
            statusImageHandler.email = ErrorImage;
            statusImageTitleHandler.email = statusImageTitleMessages.email.duplicationError;
        }
        // if all form input values are confirmed check
        else if (!formValidationCheck) {
            return;
        }
        // creating new account with new id and registration Date
        else {
            newAccountTemplate.id = nextId;
            newAccountTemplate.registration = new Date();
            dispatch(RegisterNewAccount(newAccountTemplate));
            history.push('/log-in');
        };
        // updating the state in case that userName or email values already exists
        setStatusImage(statusImageHandler);
        setStatusImageTitle(statusImageTitleHandler);
    };

    return (
        <div className="createNewAccount">
            <h2>Create Account</h2>
            <form onSubmit={createNewAccount}>
                <input type="text" placeholder="UserName" name="userName" autoComplete="off" required
                    onChange={handleUserMainInput} value={newAccountTemplate.userName}></input>
                <img src={statusImage.userName} alt="status" title={statusImageTitle.userName}></img><br /><br />

                <input type="e-mail" placeholder="E-mail" name="email" autoComplete="off" required
                    onChange={handleUserMainInput}></input>
                <img src={statusImage.email} alt="status" title={statusImageTitle.email}></img><br /><br />

                <input type="e-mail" placeholder="Confirm E-mail" autoComplete="off" name="email" required
                    onChange={handleUserConfirmationInput}></input>
                <img src={statusImage.emailConfirmation} alt="status" ></img><br /><br />

                <input type="password" placeholder="Password" name="password" required
                    onChange={handleUserMainInput}></input>
                <img src={statusImage.password} alt="status" title={statusImageTitle.password}></img><br /><br />

                <input type="password" placeholder="Confirm Password" name="password" required
                    onChange={handleUserConfirmationInput}></input>
                <img src={statusImage.passwordConfirmation} alt="status"></img><br /><br />

                <button type="submit">Create</button><br /><br />
            </form>
        </div>
    );
};

export default CreateNewAccount;