import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ModifyPaymentMetod, CreatePaymentMethod, ModifyDeliveryInformations, CreateDeliveryInformations, UploadNewOrder, EmptyCart } from './Actions/index';
import ThumbUpIMG from "../Photos/thumbUp.png";

const CheckOut = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [deliveryIndex, setDeliveryIndex] = useState(0);
    const [paymentIndex, setPaymentIndex] = useState(0);
    const [confirmationContainerDisplay, setConfirmationContainerDisplay] = useState("none")
    const [errorMessage, setErrorMessage] = useState("");
    const [cvc, setCvc] = useState("");
    const [isLoaded, setIsLoaded] = useState(true);

    const cart = useSelector(state => state.Cart);
    const sessionID = useSelector(state => state.GetSession.user.id);
    const userDeliveryInformations = useSelector(state => state.GetSession.user.deliveryInformations);
    const userPaymentMethods = useSelector(state => state.GetSession.user.paymentMethods);

    const { cartProducts, cartProductsTotalPrice } = cart;
    const { firstName, lastName, address, building, app, zipCode, city, country, telNo } = userDeliveryInformations.length > 0? userDeliveryInformations[deliveryIndex] : "";
    const { cardType, cardNumber, expiryDate, cardHolderName } = userPaymentMethods.length > 0? userPaymentMethods[paymentIndex] : "";

    const [newOrderTemplate, setNewOrderTemplate] = useState({
        id: null,
        products: [],
        totalPrice: cartProductsTotalPrice,
        deliveryIndex: deliveryIndex,
        paymentIndex: paymentIndex,
        time: new Date()
    });

    const handleDeliveryInformationsSelect = (e) => {
        setDeliveryIndex(e.target.value);
        setNewOrderTemplate({ ...newOrderTemplate, deliveryIndex: Number(e.target.value) });
    };

    const handlePaymentMetodSelect = (e) => {
        setPaymentIndex(e.target.value);
        setNewOrderTemplate({ ...newOrderTemplate, paymentIndex: Number(e.target.value) });
    };

    const modifyPaymentMetod = () => {
        dispatch(ModifyPaymentMetod(userPaymentMethods[paymentIndex]));
        history.push(`/${sessionID}/modify-method/${userPaymentMethods[paymentIndex].index}`);
    };

    const createNewPaymentMethod = () => {
        dispatch(CreatePaymentMethod());
        history.push(`/${sessionID}/create-new-method`);
    };

    const modifyDeliveryInformations = () => {
        dispatch(ModifyDeliveryInformations(userDeliveryInformations[deliveryIndex]));
        history.push(`/${sessionID}/modify-delivery/${userDeliveryInformations[deliveryIndex].index}`);
    };

    const createNewDeliveryInformations = () => {
        dispatch(CreateDeliveryInformations());
        history.push(`/${sessionID}/create-new-delivery`);
    };

    const handleCvcInput = (e) => {
        setCvc(e.target.value);
    };

    const updateNewOrderValidation = () => {
        if (userDeliveryInformations.length === 0) {
            setErrorMessage("No delivery informations selected");
            return;
        }
        else if (userPaymentMethods.length === 0) {
            setErrorMessage("No payment method selected");
            return;
        }
        else if (cvc.length !== 3) {
            setErrorMessage("Cvc not valid!");
            return;
        }
        else {
            setErrorMessage("");
            updateNewOrder();
        };
    };

    const updateNewOrder = () => {

        setConfirmationContainerDisplay("flex")

        // storing vital informations of the products in newOrderTemplate object
        cartProducts.forEach(product => {
            let productHandler = {};
            productHandler.id = product.id;
            productHandler.quantity = product.quantity;
            productHandler.price = product.currentPrice;

            newOrderTemplate.products = [...newOrderTemplate.products, productHandler];
        });
        newOrderTemplate.id = Date.now();

        dispatch(UploadNewOrder(newOrderTemplate));

        // simulation of payment validation 
        setTimeout(() => {
            dispatch(EmptyCart());
            setIsLoaded(false);
        }, 3000);

        setTimeout(() => {
            history.push(`/${sessionID}/order/${newOrderTemplate.id}`);
        }, 6000);
    };

    return (
        <div className="checkOutContainer">
            <div className="confirmationContainer" style={{ display: confirmationContainerDisplay }}>
                {isLoaded ?
                    <div className="loadingContainer"></div> :
                    <div className="paymentConfirmation">
                        Your order has been successfully processed
                        <img src={ThumbUpIMG} alt="OrderConfirmationImage"></img>
                    </div>}
            </div>
            <div className="checkOutContainerForm">
                <div className="checkOutDeliveryInformations">
                    <h2>DELIVERY INFORMATIONS</h2>
                    {!userDeliveryInformations.length > 0 ?
                        <button onClick={createNewDeliveryInformations}>CREATE NEW TEMPLATE</button> :
                        <div className="checkOutDeliveryInformations">
                            <select onChange={handleDeliveryInformationsSelect}>
                                {userDeliveryInformations.map(deliveryInformations => <option key={deliveryInformations.index}
                                    value={deliveryInformations.index}>{`${deliveryInformations.firstName} ${deliveryInformations.lastName}`}, {deliveryInformations.address}
                                </option>)}
                            </select>
                            <input type="text" readOnly value={firstName} placeholder="FirstName"></input> <br />
                            <input type="text" readOnly value={lastName} placeholder="LastName"></input> <br />
                            <input type="text" readOnly value={address} placeholder="Delivery Adress"></input><br />
                            <div className="checkOutDeliveryInformationsAdditionToAdress">
                                <input type="text" readOnly value={building} placeholder="Building"></input>
                                <input type="text" readOnly value={app} placeholder="App No."></input>
                            </div><br />
                            <input type="text" readOnly value={zipCode} placeholder="Zip Code"></input><br />
                            <input type="text" readOnly value={city} placeholder="City"></input><br />
                            <input type="text" readOnly placeholder="Region"></input><br />
                            <input type="text" readOnly value={country} placeholder="Country"></input><br />
                            <input type="text" readOnly value={telNo} placeholder="Telephone Number"></input><br /><br /><br />
                            <button onClick={modifyDeliveryInformations}>MODIFY THIS TEMPLATE</button><br />
                            <button onClick={createNewDeliveryInformations}>CREATE NEW TEMPLATE</button><br />
                        </div>}
                </div>
                <div className="checkOutPayment">
                    <h2>PAYMENT METHOD</h2>
                    {!userPaymentMethods.length > 0 ?
                        <button onClick={createNewPaymentMethod}>CREATE NEW TEMPLATE</button> :
                        <div className="checkOutPayment">
                            <select onChange={handlePaymentMetodSelect}>
                                {userPaymentMethods.map(paymentMethod => <option key={paymentMethod.cardNumber}
                                    value={paymentMethod.index}>{`${paymentMethod.cardType}`}, {paymentMethod.cardNumber}
                                </option>)}
                            </select>
                            <input type="text" readOnly value={cardType} placeholder="Card Number" ></input> <br />
                            <input type="text" readOnly value={cardNumber} placeholder="Card Number" ></input> <br />
                            <input type="text" readOnly value={expiryDate} placeholder="Expired Date"></input> <br />
                            <input type="password" autoFocus placeholder="cvc" onChange={handleCvcInput} maxLength={3} value={cvc}></input> <br />
                            <input type="text" readOnly value={cardHolderName} placeholder="CardHolder Name"></input> <br /><br /><br />
                            <button onClick={modifyPaymentMetod}>MODIFY THIS TEMPLATE</button><br />
                            <button onClick={createNewPaymentMethod}>CREATE NEW TEMPLATE</button><br />
                        </div>}
                </div>
            </div>
            <h1>TOTAL</h1>
            <h1>{`${cartProductsTotalPrice}€`}</h1>
            <p style={{ color: "red", fontWeight: "bold", fontSize: "20px" }}>{errorMessage}</p>
            <button onClick={updateNewOrderValidation} style={{ marginBottom: "50px" }}>PAY</button>
        </div>
    );
};

export default CheckOut;