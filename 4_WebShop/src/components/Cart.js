import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import CartProductCard from './CartProductCard';

function Cart() {

    const history = useHistory();

    const cart = useSelector(state => state.Cart);
    const isLogged = useSelector(state => state.GetSession.isLogged);

    const { cartProducts, cartProductsTotalPrice } = cart;

    return (
        <div className="cartContainer">
            {cartProducts.map(product => <CartProductCard key={product.id} product={product} />)}
            {cartProductsTotalPrice !== 0 ?
                <div className="cartWithItems">
                    <h2>TOTAL: {`${cartProductsTotalPrice}€`} </h2><br />
                    <button onClick={() => isLogged ? history.push("/cart/check-out") : history.push({ pathname: "/log-in", state: true })}>CheckOut</button>
                </div> :
                <div className="emptyCart">
                    <p>The Cart is empty!</p>
                    <p>Continue to <button onClick={() => history.push("/store")}>STORE</button></p>
                </div>}
        </div>
    );
};

export default Cart;
