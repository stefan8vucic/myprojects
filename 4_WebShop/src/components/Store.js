import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import { useSelector } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';

function Store() {

    const queryString = require('query-string');

    const location = useLocation();
    const history = useHistory();

    const allProducts = useSelector(state => state.Products);

    // making query string object from location.search 
    const [query, setQuery] = useState(queryString.parse(location.search));

    // setting filters from query object
    const [selectedBrand, setSelectedBrand] = useState(query.brand ? query.brand : "");
    const [searchInput, setSearcInput] = useState(query.input ? query.input : "");
    const [selectedPrice, setSelectedPrice] = useState(query.price ? query.price : 0);

    const [allBrands, setAllBrands] = useState([]);
    const [displayProducts, setDisplayProducts] = useState([]);

    const [maxPrice, setMaxPrice] = useState(0);
    const [minPrice, setMinPrice] = useState(0);

    // updating all brands and products to be displayed with their min and max price on initial mount of the component
    useEffect(() => {
        getBrands();
        getPrices();
        updateDisplayProducts(query.brand, query.input);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    // getting all brands from allProducts to update "select brand" element 
    const getBrands = () => {
        let brands = [];
        allProducts.forEach(product => {
            if (!brands.includes(product.brand)) {
                brands = [...brands, product.brand];
            };
        });
        setAllBrands([...brands]);
    };

    const handleBrandSelection = (e) => {
        setSelectedBrand(e.target.value);
        updateQueryStringObject("brand", e.target.value);
    };

    const handleSearchInput = (e) => {
        setSearcInput(e.target.value);
        updateQueryStringObject("input", e.target.value);
    };

    // on any change of "displayProducts" updating min and max price 
    useEffect(() => {
        getPrices();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [displayProducts]);

    // getting min and max price to update min and max value of price range element
    const getPrices = () => {

        if (displayProducts.length > 0) {
            let prices = [];
            displayProducts.forEach(item => {
                prices = [...prices, item.currentPrice]
            });

            setMaxPrice(Math.max(...prices));
            setMinPrice(Math.min(...prices));
            // if query.price is in min and max price range of "displayProducts" 
            if(query.price > Math.min(...prices) && query.price < Math.max(...prices)) {
                setSelectedPrice(query.price);
            }
            // otherwise "selected Price" will be equal to max price and property price will be deleted from query object
            // example: 1) select brand = all & selected price = 400 2) select brand = SAMSUNG --> selected price < SAMSUNG min price
            else {
                setSelectedPrice(Math.max(...prices));
                updateQueryStringObject("price", false);
            }
        }
        else {
            setMaxPrice(0);
            setMinPrice(0);
            setSelectedPrice(0);
        };
    };

    const handleRangePriceSelect = (e) => {
        setSelectedPrice(Number(e.target.value));
        updateQueryStringObject("price", Number(e.target.value));
    }

    // for any change of selectedBrand or searchInput, updateDisplayProducts will be triggered
    useEffect(() => {
        updateDisplayProducts(selectedBrand, searchInput);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedBrand, searchInput]);

    const updateDisplayProducts = (brand, input) => {
        if (brand && input) {
            let selectedBrandProducts = allProducts.filter(item => item.brand.toLowerCase() === brand);
            setDisplayProducts(selectedBrandProducts.filter(item => (`${item.brand} ${item.model}`).toLocaleLowerCase().includes(searchInput.toLowerCase())));
        }
        else if (brand && !input) {
            setDisplayProducts(allProducts.filter(item => item.brand.toLowerCase() === brand));
        }
        else if (!brand && input) {
            setDisplayProducts(allProducts.filter(item => (`${item.brand} ${item.model}`).toLocaleLowerCase().includes(searchInput.toLowerCase())));
        }
        else {
            setDisplayProducts(allProducts);
        };
    };

    // updating query object with selected filters
    const updateQueryStringObject = (property, value) => {
        let queryHandler = { ...query };
        // in case that value === maxPrice, property: price will be deleted from query object 
        if (!value || value === maxPrice) {
            delete queryHandler[property];
        }
        else {
            queryHandler[property] = value;
        }
        setQuery(queryHandler);
    }

    // for any change of query object handleRouterURL will update aplication url
    // in learning purpose the url will update even at any searchInput change 
    useEffect(() => {
        handleRouterURL();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    const handleRouterURL = () => {
        history.replace({ pathname: `/store`, search: queryString.stringify(query) });
    }

    return (
        <div className="storeContainer">
            <div className="storeNavigation">
                <div className="storeSearchContainer">
                    <img src={require('../Photos/search.png')} alt="SearchIcon"></img>
                    <input value={searchInput} type="search" autoComplete="off" onChange={handleSearchInput}></input>
                </div>
                <div className="priceSlideContainer">
                    <p>{`${selectedPrice}€`}</p>
                    <div className="priceSliderContainer">
                        {`${minPrice}€`}
                        <input id="priceSlide" type="range"
                            min={minPrice}
                            max={maxPrice} value={selectedPrice} onChange={handleRangePriceSelect} style={{ height: "1px" }}></input >
                        {`${maxPrice}€`}
                    </div>
                </div>
                <div className="selectBrandContainer">
                    Select brand <br />
                    <select onChange={handleBrandSelection} value={query.brand}>
                        <option value="">--ALL--</option>
                        {allBrands.map(brand => <option key={brand} value={brand.toLowerCase()}>{brand}</option>)}
                    </select>
                </div>
            </div>
            <div className="storeProducts">
                {displayProducts.length > 0 ?
                    displayProducts.filter(product => product.currentPrice <= selectedPrice)
                        .map(product => <Link key={product.id} to={`/item/${product.path}`}><ProductCard product={product} /> </Link>) : 
                        <p style={{fontSize: "25px", fontWeight: "bold"}}>No results</p>}
            </div>
        </div>
    );
};

export default Store;
