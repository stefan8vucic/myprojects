import React from 'react';
import { useSelector } from 'react-redux';
import { Link, useHistory } from "react-router-dom";
import ShoppingHistoryCard from './ShoppingHistoryCard';

function ShoppingHistory() {

    const history = useHistory();

    const userShoppingHistory = useSelector(state => state.GetSession.user.shoppingHistory);
    const sessionID = useSelector(state => state.GetSession.user.id);

    return (
        <>
            {userShoppingHistory.length === 0 ?
                <>
                    <p style={{ fontSize: "25px", fontWeight: "bold" }}>No shoping history</p> <br />
                    <p style={{ fontSize: "22px", fontWeight: "bold" }}>Continue to <button onClick={() => history.push("/store")}>STORE</button></p>
                </> :
                <div className="shoppingHistoryContainer">
                    {userShoppingHistory.map(shoppingHistory =>
                        <Link to={`/${sessionID}/order/${shoppingHistory.id}`} key={shoppingHistory.id}>
                            <ShoppingHistoryCard shoppingHistory={shoppingHistory} />
                        </Link>)}
                </div>}
        </>
    );
};

export default ShoppingHistory;