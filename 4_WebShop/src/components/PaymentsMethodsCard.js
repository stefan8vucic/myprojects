import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ModifyPaymentMetod } from './Actions/index';
import DeleteImage from '../Photos/delete.png';
import EditImage from '../Photos/edit.png';

function PaymentsMethodsCard(props) {

    const dispatch = useDispatch();
    const history = useHistory();

    const sessionID = useSelector(state => state.GetSession.user.id);

    const { index, cardType, cardNumber, expiryDate } = props.paymentMethod;

    const modifyPaymentMetod = () => {
        dispatch(ModifyPaymentMetod(props.paymentMethod));
        history.push(`/${sessionID}/modify-method/${index}`);
    };

    return (
        <div className="paymentMethodCardContainer">
            {cardType} <br />
            {cardNumber} <br />
            {expiryDate}
            <img onClick={() => props.delete(index)} src={DeleteImage} alt="Delete" title="Delete"></img>
            <img onClick={modifyPaymentMetod} src={EditImage} alt="Edit" title="Edit"></img>
        </div>
    );
};

export default PaymentsMethodsCard;