import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { CreateDeliveryInformations, DeleteDeliveryInformations } from './Actions/index';
import DeliveryInformationsCard from './DeliveryInformationsCard';

function DeliveryInformations() {

    const userDeliveryInformations = useSelector(state => state.GetSession.user.deliveryInformations);
    const sessionID = useSelector(state => state.GetSession.user.id);

    const history = useHistory();
    const dispatch = useDispatch();

    const [deleteTemplateConfirmationDisplay, setDeleteTemplateConfirmationDisplay] = useState('none');
    const [deleteTemplateIndex, setDeleteTemplateIndex] = useState(0);

    const createNewDeliveryInformations = () => {
        dispatch(CreateDeliveryInformations());
        history.push(`/${sessionID}/create-new-delivery`);
    };

    const deleteDeliveryInformationsTemplate = (index) => {
        setDeleteTemplateConfirmationDisplay("flex");
        setDeleteTemplateIndex(index);
    };
    // this will render confirmation window 
    const deleteDeliveryInformationsTemplateConfirmation = (index) => {
        dispatch(DeleteDeliveryInformations(index));
        setDeleteTemplateConfirmationDisplay("none");
        history.push(`/${sessionID}/deliveries`);
    };

    return (
        <>
            <div className="confirmationContainer" style={{ display: deleteTemplateConfirmationDisplay }}>
                <div className="deleteTemplateConfirmation">
                    <p>You are sure that you want to delete this template?</p>
                    <button onClick={() => deleteDeliveryInformationsTemplateConfirmation(deleteTemplateIndex)} style={{ width: "120px" }}>DELETE</button>
                    <button onClick={() => setDeleteTemplateConfirmationDisplay("none")} style={{ width: "120px" }}>CANCEL</button>
                </div>
            </div>
            <div className="addressesInformationsContainer">
                {userDeliveryInformations ?
                    userDeliveryInformations.map(deliveryInformations =>
                        <DeliveryInformationsCard key={deliveryInformations.index} deliveryInformations={deliveryInformations}
                            delete={deleteDeliveryInformationsTemplate} />) : ""}
                <button onClick={createNewDeliveryInformations}>CREATE NEW TEMPLATE</button>
            </div>
        </>
    );
};

export default DeliveryInformations;