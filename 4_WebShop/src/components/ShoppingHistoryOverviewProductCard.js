import React from "react";

function ShoppingHistoryOverviewProductCard(props) {

    const {info : { brand, model, image }, quantity, price } = props.product;

    return (
        <div className="shoppingHistoryOverviewProductCardContainer">
            <img src={image} alt="ProductImage"></img>
            <div>
                {brand}<br />
                {model}
            </div>
            <div>
                <strong>Qty.</strong><br /><br />
                {quantity}
            </div>
            <div>
                <strong>Unit price</strong><br /><br />
                {`${price}€`}
            </div>
            <div>
                <strong>Total</strong><br /><br />
                {`${quantity * price}€`}
            </div>
        </div>
    );
};

export default ShoppingHistoryOverviewProductCard;