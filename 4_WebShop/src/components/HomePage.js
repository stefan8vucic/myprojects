import React from 'react';
import { useHistory } from 'react-router-dom';
import PromoIMG from '../Photos/promotion.jpg';
import S10plusIMG from '../Photos/galaxy-s10.jpg';

function HomePage(){

    const history = useHistory();

    return(
        <div className="homePage">
            <div className="homePagePromotionContainer">
                <img src={S10plusIMG} alt="Promo" onClick={() => history.push("/item/samsung-s10+")}></img>
                <img src={PromoIMG} alt="Promo" onClick={() => history.push("/store?brand=huawei")}></img>
            </div>
        </div>
    );
};

export default HomePage;
