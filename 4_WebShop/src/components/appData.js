const validation = {
    userName: /^[A-Za-z0-9]{4,12}$/,
    email: /^([A-Z0-9a-z._%+-]{2,})+@[A-Z0-9a-z.-]+\.[A-Za-z]{2,}$/,
    password: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,18}$/
};

const statusImageTitleMessages = {
    userName: {
        default: `*4 characters min \n*12 characters max \n*A-Z, a-z, 0-9 allowed`,
        typingError: `*4 characters min \n*12 characters max \n*A-Z, a-z, 0-9 allowed`,
        duplicationError: "This UserName already exists"
    },
    email: {
        default: "",
        typingError: "E-mail format not valid",
        duplicationError: "This E-mail already exists"
    },
    password: {
        default: `*6 characters min \n*18 characters max \n*1 uppercase - required \n*1 lowercase - required \n*1 number - required`,
        typingError: `*6 characters min \n*18 characters max \n*1 uppercase - required \n*1 lowercase - required \n*1 number - required`
    }
};

const users = [
    {
        id: 1000001,
        isLogged: false,
        userName: "Stefan",
        email: "stefan8vucic@gmail.com",
        password: "1",
        paymentMethods: [{
            index: 0,
            cardType: "Visa",
            cardNumber: 1234123412341234,
            expiryDate: "12/22",
            cardHolderName: "Stefan Vucic",
            creationDate: new Date(2020, 4, 23, 21, 34, 9)
        },
        {
            index: 1,
            cardType: "Maestro",
            cardNumber: 123416565656234,
            expiryDate: "02/28",
            cardHolderName: "Stefan Vucic",
            creationDate: new Date(2020, 4, 23, 21, 34, 9)
        }
        ],
        deliveryInformations: [
            {
                index: 0,
                firstName: "Stefan",
                lastName: "Vucic",
                address: "223 Avenu d'Ares",
                building: "U1",
                app: 21,
                zipCode: 33200,
                city: "Bordeaux",
                country: "France",
                telNo: "0033642792236"
            },
            {
                index: 1,
                firstName: "Stefan",
                lastName: "Vucic",
                address: "Jagare 1",
                building: "",
                app: "",
                zipCode: 78000,
                city: "Banja Luka",
                country: "Bosnia and Herzegovina",
                telNo: "0038765239113"
            }
        ],
        shoppingHistory: [
            {
                id: 1601989551665,
                products: [{ id: 10001, quantity: 1, price: 399 }, { id: 10003, quantity: 1, price: 400 }],
                totalPrice: 799,
                deliveryIndex: 1,
                paymentIndex: 0,
                time: new Date(2020, 8, 22, 21, 13, 45)
            },
            {
                id: 1601989567234,
                products: [{ id: 10002, quantity: 1, price: 259 }, { id: 10005, quantity: 1, price: 499 }],
                totalPrice: 758,
                deliveryIndex: 0,
                paymentIndex: 1,
                time: new Date(2020, 9, 14, 14, 22, 54)
            }
        ]
    }
];

const products = [
    {
        id: 10001,
        status: false,
        brand: "HUAWEI",
        model: "P30",
        currentPrice: 367,
        oldPrice: 399,
        discount: "8%",
        image: require('../Photos/P30.png'),
        details: {
            display: '6.1"',
            frontCamera: "40/16/8 Mpx",
            rareCamera: "32Mpx",
            ram: "6GB",
            memory: "128GB",
            weight: "165 g",
            battery: "3650 mAh"
        },
        path: "huawei-p30"
    },
    {
        id: 10002,
        status: false,
        brand: "HUAWEI",
        model: "P30-lite",
        currentPrice: 225,
        oldPrice: 259,
        discount: "13%",
        image: require('../Photos/P30 lite.png'),
        details: {
            display: '6.15"',
            frontCamera: "48/8/2 Mpx",
            rareCamera: "32 Mpx",
            ram: "4 GB",
            memory: "128 GB",
            weight: "159 g",
            battery: "3340 mAh"
        },
        path: "huawei-p30-lite"
    },
    {
        id: 10003,
        status: false,
        brand: "IPHONE",
        model: "8+",
        currentPrice: 400,
        oldPrice: "",
        image: require('../Photos/iPhone 8 plus.png'),
        details: {
            display: '5.5"',
            frontCamera: "12/12 Mpx",
            rareCamera: "7 Mpx",
            ram: "3 GB",
            memory: "64 GB",
            weight: "202 g",
            battery: "2691 mAh"
        },
        path: "iphone-8+"
    },
    {
        id: 10004,
        status: false,
        brand: "IPHONE",
        model: "X",
        currentPrice: 483,
        oldPrice: "",
        image: require('../Photos/iPhone X.png'),
        details: {
            display: '5.8"',
            frontCamera: "12/12 Mpx",
            rareCamera: "7 Mpx",
            ram: "3 GB",
            memory: "64 GB",
            weight: "174 g",
            battery: "2716 mAh"
        },
        path: "iphone-x"
    },
    {
        id: 10005,
        status: true,
        brand: "SAMSUNG",
        model: "S10+",
        currentPrice: 499,
        oldPrice: "",
        image: require('../Photos/S10+.png'),
        details: {
            display: '6.4"',
            frontCamera: "12/12/16 Mpx",
            rareCamera: "10/8 Mpx",
            ram: "8 GB",
            memory: "128 GB",
            weight: "175 g",
            battery: "4100 mAh"
        },
        path: "samsung-s10+"
    },
    {
        id: 10006,
        status: false,
        brand: "SAMSUNG",
        model: "S10",
        currentPrice: 480,
        oldPrice: "",
        image: require('../Photos/S10.png'),
        details: {
            display: '6.1"',
            frontCamera: "12/12/16 Mpx",
            rareCamera: "10 Mpx",
            ram: "8 GB",
            memory: "128 GB",
            weight: "157 g",
            battery: "3400 mAh"
        },
        path: "samsung-s10"
    }];

export { validation, statusImageTitleMessages, users, products };
