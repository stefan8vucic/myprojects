import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Header from './Header';
import HomePage from './HomePage';
import Footer from './Footer';
import Store from './Store';
import Contact from './Contact';
import Cart from './Cart';
import Account from './UserAccount';
import ProductProfile from './ProductProfile';
import LogIn from './LogIn';
import CreateNewAccount from './CreateNewAccount';
import CheckOut from './CheckOut';

function App() {

  return (
    <Router>
      <div className="container">
        <Header />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/store" exact component={Store} />
          <Route path="/item/:path" component={ProductProfile} />
          <Route path="/contact" component={Contact} />
          <Route path="/cart" exact component={Cart} />
          <Route path="/cart/check-out" component={CheckOut} />
          <Route path="/log-in" component={LogIn} />
          <Route path="/create-new-account" exact component={CreateNewAccount} />
          <Route path="/:userID" component={Account} />
        </Switch>
        <Footer />
      </div>
    </Router>
  );
};

export default App;
