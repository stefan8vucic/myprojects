import React from "react";
import { useSelector } from "react-redux";
import ShoppingHistoryOverviewProductCard from "./ShoppingHistoryOverviewProductCard";
import WebShopLogo from '../Photos/logo_dark.png';

function ShoppingHistoryOverview({ match }) {

    const { orderID } = match.params;

    const allProducts = useSelector(state => state.Products);
    const userShoppingHistory = useSelector(state => state.GetSession.user.shoppingHistory);
    const userPaymentMethods = useSelector(state => state.GetSession.user.paymentMethods);
    const userDeliveryInformations = useSelector(state => state.GetSession.user.deliveryInformations);

    const displayOrder = userShoppingHistory.filter(order => order.id === Number(orderID))[0];

    const { id, products, totalPrice, deliveryIndex, paymentIndex, time } = displayOrder;

    // updating displayOrder products objects with infos(brand, model, image...) to be render in ShoppingHistoryOverviewProductCard component
    products.forEach(displayOrderProduct => {
        displayOrderProduct.info = allProducts.filter(product => product.id === displayOrderProduct.id)[0];
    });

    const orderPaymentMethod = userPaymentMethods[paymentIndex];
    const { cardType, cardNumber, cardHolderName } = orderPaymentMethod;

    const orderDeliveryInformations = userDeliveryInformations[deliveryIndex];
    const { firstName, lastName, address, building, app, zipCode, city, country, telNo } = orderDeliveryInformations;

    return (
        <div className="shoppingHistoryOverviewContainer">
            <div className="shoppingHistoryOverviewHeader">
                <img src={WebShopLogo} alt="WebShopLogo"></img>
                <p>ORDER CONFIRMATION</p>
            </div>
            {products.map(product => <ShoppingHistoryOverviewProductCard key={product.id} product={product} />)}
            <h2>{`Order no: ${id}`}</h2>
            <div className="customerInfo">
                <div className="deliveryInformations">
                    <strong>Delivery Informations</strong><br /><br />
                    {firstName} {lastName} <br />
                    {address} <br />
                    {`${building} ${app}`} <br />
                    {zipCode} {city} <br />
                    {country} <br />
                    {telNo}
                </div>
                <div className="paymentInformations">
                    <strong>Payment Informations</strong> <br /> <br />
                    {cardHolderName} <br />
                    {cardType} <br />
                    {cardNumber}
                </div>
            </div>
            <div className="orderInfo">
                <div className="orderDate">
                    <strong>Order Date</strong><br /><br />
                    {time.toLocaleTimeString()}<br />
                    {time.toLocaleDateString()}
                </div>
                <div className="orderTotal">
                    <strong>TOTAL</strong> <br /> <br />
                    {`${totalPrice}€`}
                </div>
            </div>
        </div>
    );
};

export default ShoppingHistoryOverview;