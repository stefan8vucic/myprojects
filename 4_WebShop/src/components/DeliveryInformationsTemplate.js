import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { UploadModifiedDeliveryInformations, UploadNewDeliveryInformations } from './Actions/index';

function DeliveryInformationsTemplate() {

    const history = useHistory();
    const dispatch = useDispatch();

    const deliveryInformationsTemplate = useSelector(state => state.GetSession.deliveryInformationsTemplate);

    const [newDeliveryInformations, setNewDeliveryInformations] = useState({ ...deliveryInformationsTemplate });

    const handleNewDeliveryInformationsTemplate = (e) => {
        setNewDeliveryInformations({
            ...newDeliveryInformations,
            [e.target.name]: e.target.value
        });
    };

    const uploadNewDeliveryInformations = (e) => {
        e.preventDefault();
        newDeliveryInformations.creationDate = new Date();
        dispatch(UploadNewDeliveryInformations(newDeliveryInformations));
        history.goBack();
    };

    const uploadModifiedDeliveryInformations = (e) => {
        e.preventDefault();
        dispatch(UploadModifiedDeliveryInformations(newDeliveryInformations));
        history.goBack();
    };

    const { firstName, lastName, address, building, app, zipCode, city, region, country, telNo } = deliveryInformationsTemplate;

    return (
        <div className="templateContainer">
            <form onSubmit={firstName ? uploadModifiedDeliveryInformations : uploadNewDeliveryInformations}>
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={firstName} name="firstName" autoComplete="off" placeholder="FirstName" required></input> <br />
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={lastName} name="lastName" autoComplete="off" placeholder="LastName" required></input> <br />
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={address} name="address" autoComplete="off" placeholder="Delivery Adress" required></input><br />
                <div className="templateContainerAdditionToAdress">
                    <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={building} name="building" autoComplete="off" placeholder="Building"></input>
                    <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={app} name="app" autoComplete="off" placeholder="App No."></input>
                </div>
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={zipCode} name="zipCode" autoComplete="off" placeholder="Zip Code" required></input><br />
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={city} name="city" autoComplete="off" placeholder="City" required></input><br />
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={region} name="region" autoComplete="off" placeholder="Region"></input><br />
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={country} name="country" autoComplete="off" placeholder="Country" required></input><br />
                <input onChange={handleNewDeliveryInformationsTemplate} type="text" defaultValue={telNo} name="telNo" autoComplete="off" placeholder="Telephone Number" required></input> <br />
                <button type="submit">{firstName ? "SAVE" : "CREATE"} </button>
            </form>
        </div>
    );
};

export default DeliveryInformationsTemplate;