import * as Action from './type.js';

export const AddToCart = (product) => {
    return {
        type: Action.ADD_TO_CART,
        payload: product
    };
};

export const ReduceCartQuantity = (product) => {
    return {
        type: Action.REDUCE_CART_QUANTITY,
        payload: product
    };
};

export const RemoveProductFromCart = (product) => {
    return {
        type: Action.REMOVE_PRODUCT_FROM_CART,
        payload: product
    };
};

export const EmptyCart = () => {
    return {
        type: Action.EMPTY_CHART
    };
};

export const RegisterNewAccount = (account) => {
    return {
        type: Action.REGISTER_NEW_ACCOUNT,
        payload: account
    };
};

export const GetSession = (user) => {
    return {
        type: Action.GET_SESSION,
        payload: user
    };
};

export const LogOut = () => {
    return {
        type: Action.LOG_OUT,
    };
};

export const CreatePaymentMethod = () => {
    return {
        type: Action.CREATE_PAYMENT_METHOD,
    };
};

export const ModifyPaymentMetod = (paymentMethod) => {
    return {
        type: Action.MODIFY_PAYMENT_METHOD,
        payload: paymentMethod
    };
};

export const UploadModifiedPaymentMetod = (paymentMethod) => {
    return {
        type: Action.UPLOAD_MODIFIED_PAYMENT_METHOD,
        payload: paymentMethod
    };
};

export const UploadNewPaymentMetod = (paymentMethod) => {
    return {
        type: Action.UPLOAD_NEW_PAYMENT_METHOD,
        payload: paymentMethod
    };
};

export const DeletePaymentMethod = (index) => {
    return {
        type: Action.DELETE_PAYMENT_METHOD,
        payload: index
    };
};

export const CreateDeliveryInformations = () => {
    return {
        type: Action.CREATE_DELIVERY_INFORMATIONS,
    };
};

export const ModifyDeliveryInformations = (deliveryInformations) => {
    return {
        type: Action.MODIFY_DELIVERY_INFORMATIONS,
        payload: deliveryInformations
    };
};

export const UploadModifiedDeliveryInformations = (deliveryInformations) => {
    return {
        type: Action.UPLOAD_MODIFIED_DELIVERY_INFORMATIONS,
        payload: deliveryInformations
    };
};

export const UploadNewDeliveryInformations = (deliveryInformations) => {
    return {
        type: Action.UPLOAD_NEW_DELIVERY_INFORMATIONS,
        payload: deliveryInformations
    };
};

export const DeleteDeliveryInformations = (index) => {
    return {
        type: Action.DELETE_DELIVERY_INFORMATIONS,
        payload: index
    };
};

export const UploadNewOrder = (newOrder) => {
    return {
        type: Action.UPLOAD_NEW_ORDER,
        payload: newOrder
    };
};