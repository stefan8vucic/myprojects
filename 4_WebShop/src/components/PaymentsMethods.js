import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { CreatePaymentMethod, DeletePaymentMethod } from './Actions/index';
import PaymentsMethodsCard from './PaymentsMethodsCard';

function PaymentsMethods() {

    const history = useHistory();
    const dispatch = useDispatch();

    const userPaymentMethods = useSelector(state => state.GetSession.user.paymentMethods);
    const sessionID = useSelector(state => state.GetSession.user.id);

    const [deleteTemplateConfirmationDisplay, setDeleteTemplateConfirmationDisplay] = useState('none');
    const [deleteTemplateIndex, setDeleteTemplateIndex] = useState(0);

    const createNewPaymentMethod = () => {
        dispatch(CreatePaymentMethod());
        history.push(`/${sessionID}/create-new-method`);
    };
    // this will render confirmation window 
    const deletePaymentMethodTemplate = (index) => {
        setDeleteTemplateConfirmationDisplay("flex");
        setDeleteTemplateIndex(index);
    };

    const deletePaymentMethodTemplateConfirmation = (index) => {
        dispatch(DeletePaymentMethod(index));
        setDeleteTemplateConfirmationDisplay("none");
        history.push(`/${sessionID}/payment-methods`);
    };

    return (
        <>
            <div className="confirmationContainer" style={{ display: deleteTemplateConfirmationDisplay }}>
                <div className="deleteTemplateConfirmation">
                    <p>You are sure that you want to delete this template?</p>
                    <button onClick={() => deletePaymentMethodTemplateConfirmation(deleteTemplateIndex)} style={{ width: "120px" }}>DELETE</button>
                    <button onClick={() => setDeleteTemplateConfirmationDisplay("none")} style={{ width: "120px" }}>CANCEL</button>
                </div>
            </div>
            <div className="paymentMethodsContainer">
                {userPaymentMethods ?
                    userPaymentMethods.map(paymentMethod =>
                        <PaymentsMethodsCard key={paymentMethod.cardNumber} paymentMethod={paymentMethod}
                            delete={deletePaymentMethodTemplate} />) : ""}
                <button onClick={createNewPaymentMethod}>CREATE NEW TEMPLATE</button>
            </div>
        </>
    );
};

export default PaymentsMethods;