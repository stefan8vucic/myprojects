//main dom elements
const citySearchContainer = document.getElementById("citySearchContainer");
const navigationContainer = document.getElementById("navigationContainer");
const weatherDataContainer = document.getElementById("weatherDataContainer");
// city search DOM elements
const citySearchInput = document.getElementById("citySearchInput");
const citySearchResults = document.getElementById("citySearchResults");
// main weather DOM elements
const weatherStatusImage = document.getElementById("weatherStatusImage");
const mainWeatherStatus = document.getElementById("mainWeatherStatus");
const mainWeatherDescription = document.getElementById("mainWeatherDescription");
// location DOM elements
const cityName = document.getElementById("city");
const countryName = document.getElementById("country");
const displayTime = document.getElementById("time");
const displayDate = document.getElementById("date");
// sunrise and sunset DOM elements
const sunriseTime = document.getElementById("sunrise");
const sunsetTime = document.getElementById("sunset");
// weather DOM elements
const currentTemperature = document.getElementById("currentTemperature");
const currentTemperatureDecimal = document.getElementById("currentTemperatureDecimal");
const feelsLike = document.getElementById("feelsLike");
const feelsLikeDecimal = document.getElementById("feelsLikeDecimal");
const displayPressure = document.getElementById("pressure");
const displayHumidity = document.getElementById("humidity");
const windSpeed = document.getElementById("windSpeed");
const windDirectionImage = document.getElementById("windDirectionImage");
const windDirection = document.getElementById("windDirection");
// "threeHours" weather selection DOM element
const threeHoursWeather = document.getElementById("threeHoursWeatherContainer");

const weatherStatusImages = ["./Images/thunderstorm.png", "./Images/light_rain.png", "./Images/extreme_rain.png", "./Images/snow.png", "./Images/fog.png", "./Images/tornado.png", "./Images/sunny.png", "./Images/night.png", "./Images/night_clouds.png", "./Images/few_clouds.png", "./Images/clouds.png"];

let initialRender = true;

let chartData = {
    temp: [16, 15, 16, 16, 15, 16],
    precipitation: [0.5, 0, 0.5, 0.5, 0, 0.5],
    labels: ["0", "3", "6", "9", "12", "15"]
};

let weatherData = {
    currentWeather: {},
    sevenDaysWeather: [],
    fiveDaysThreeHoursWeather: [],
    alerts: [],
    timezone_offset: null
};

let locationData = {};

let appSettingsData = {
    units: "metric",
    timeZone: "local"
};

let currentWeatherDisplayed = true;

const handleCitySearchEvents = (e) => {
    // on focus or input event, results container will be displayed if input element includes at least one character
    if (e.type === "focus") {
        if (e.target.value.trim()) {
            // getCities(e)
            citySearchResults.style.display = "flex";
        }
        else {
            citySearchResults.style.display = "none";
        };
    }
    // on blur event search container will not be displayed,
    // setTimeout awaits blur event in case that city is selected
    else if (e.type === "blur") {
        setTimeout(() => {
            citySearchResults.style.display = "none";
        }, 400);
    };
};

citySearchInput.addEventListener("blur", handleCitySearchEvents);
citySearchInput.addEventListener("focus", handleCitySearchEvents);

// fetching data from GeoDB Cities REST API
const getCities = async (e) => {
    if (e.target.value) {
        try {
            const response = await fetch(`https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=${e.target.value}`, {
                "method": "GET",
                "headers": {
                    "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
                    "x-rapidapi-key": "331ced091bmshfde2c851893af17p18fddcjsn12de90468af9"
                }
            });
            // check the status of the response
            checkFetchStatus(response);
            const data = await response.json();
            updateCitySearchResultsInDOM(data.data);
        }
        // because of 1 request per second limit, error will render "keep typing" 
        catch (err) {
            citySearchResults.innerHTML = null;
            const divElement = createNewElement("div", `keep typing...`);
            citySearchResults.appendChild(divElement);
        };
    }
    else {
        citySearchResults.style.display = "none";
        return;
    };
};

citySearchInput.addEventListener("input", (e) => debounce_handler(e));

// updating results for searched location in DOM
const updateCitySearchResultsInDOM = (data) => {
    citySearchResults.innerHTML = null;
    if (data.length > 0) {
        data.forEach(location => {
            const divElement = createNewElement("div", `${location.city} - ${location.country}`);
            divElement.addEventListener("click", () => {
                getWeather(location);
                initialWeatherDataRender(initialRender);
            });
            citySearchResults.appendChild(divElement);
        });
        citySearchResults.style.display = "flex";
    }
    else {
        const divElement = createNewElement("div", `no results`);
        citySearchResults.appendChild(divElement);
    };
};

const initialWeatherDataRender = (status) => {
    // console.log(window.innerWidth)
    if (window.innerWidth > 1000) {
        if (status) {
            citySearchContainer.style.right = "0%";
            citySearchContainer.style.top = "135px";
            weatherDataContainer.style.transform = "scale(1)";
            citySearchContainer.style.transform = "translate(-23%) scale(1)";
            setTimeout(() => {

                navigationContainer.style.transform = "scale(1)";
            }, 900);

            initialRender = false;
        }
        else {
            return;
        };
    }
    else {
        if (status) {
            citySearchContainer.style.transform = "scale(0.9)";
            citySearchContainer.style.top = "120px";
            weatherDataContainer.style.transform = "scale(1)";
            setTimeout(() => {
                navigationContainer.style.transform = "scale(1)";
            }, 900);

            initialRender = false;
        }
        else {
            return;
        };
    }
};

// fetching data from Open Weather Map REST API
const getWeather = async (location) => {
    const { units } = appSettingsData;
    const currentWeatherURL = `https://api.openweathermap.org/data/2.5/onecall?lat=${location.latitude}&lon=${location.longitude}&units=${units}&exclude=hourly,minutely&appid=8204367a87c7828976991b0d1e62dbad`;
    const forecast5days3HoursURL = `https://rapidapi.p.rapidapi.com/forecast?units=${units}&lat=${location.latitude}&lon=${location.longitude}`;

    locationData = location;

    try {
        const currentWeatherResponse = await fetch(currentWeatherURL, {
            method: "GET"
        });

        const forecast5days3HoursResponse = await fetch(forecast5days3HoursURL, {
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
                "x-rapidapi-key": "331ced091bmshfde2c851893af17p18fddcjsn12de90468af9"
            }
        });

        // check the status of the responses
        checkFetchStatus(currentWeatherResponse);
        checkFetchStatus(forecast5days3HoursResponse);

        const currentWeatherData = await currentWeatherResponse.json();
        const forecast5days3HoursData = await forecast5days3HoursResponse.json();
        console.log(currentWeatherData, forecast5days3HoursData)

        updateDataWithSelectedTimeZone(currentWeatherData.current, currentWeatherData.daily, forecast5days3HoursData.list, currentWeatherData.timezone_offset, currentWeatherData.alerts);
    }
    catch (err) {
        uploadErrorInDOM();
    };
};

// check the status of the responses
const checkFetchStatus = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    else {
        return response;
    };
};

// fetch error will show weather alerts container with message and "try again" button
const uploadErrorInDOM = () => {
    weatherAlertsContent.innerText = null;
    const paragraphElement = createNewElement("p", "something went wrong...");
    const buttonElement = createNewElement("button", "Try again");
    buttonElement.addEventListener("click", () => {
        getWeather(locationData)
    });
    weatherAlertsContent.appendChild(paragraphElement);
    weatherAlertsContent.appendChild(buttonElement);
    handleWeatherAlertsContainerDisplayInDOM("show");
};

const updateDataWithSelectedTimeZone = (currentData, dailyData, hourlyData, timezone_offset, weatherAlerts) => {
    //depending on the selected time zone, dt value in all objects will be updated
    const { timeZone } = appSettingsData;
    const currentTimeZone_offset = (new Date().getTimezoneOffset()) * 60;
    const timeZonesDifference = timezone_offset + currentTimeZone_offset;
    console.log(timezone_offset, currentTimeZone_offset)
    if ((timezone_offset * -1) !== currentTimeZone_offset) {
        if (timeZone === "local") {
            currentData.dt += timeZonesDifference;
            currentData.sunrise += timeZonesDifference;
            currentData.sunset += timeZonesDifference;

            dailyData.forEach(data => {
                data.dt += timeZonesDifference;
                data.sunrise += timeZonesDifference;
                data.sunset += timeZonesDifference;
            });

            hourlyData.forEach(data => {
                data.dt += timeZonesDifference;
            });

            if (weatherAlerts) {
                weatherAlerts.forEach(date => {
                    date.start += timeZonesDifference;
                    date.end += timeZonesDifference;
                });
            };
        }
        // if the current time zone is selected after the local
        else {
            currentData.dt -= timeZonesDifference;
            currentData.sunrise -= timeZonesDifference;
            currentData.sunset -= timeZonesDifference;

            dailyData.forEach(data => {
                data.dt -= timeZonesDifference;
                data.sunrise -= timeZonesDifference;
                data.sunset -= timeZonesDifference;
            });

            hourlyData.forEach(data => {
                data.dt -= timeZonesDifference;
            });

            if (weatherAlerts) {
                weatherAlerts.forEach(date => {
                    date.start -= timeZonesDifference;
                    date.end -= timeZonesDifference;
                });
            };
        };
    };
    // updating weather data with selected timeZone format
    weatherData.currentWeather = currentData;
    weatherData.sevenDaysWeather = dailyData;
    weatherData.fiveDaysThreeHoursWeather = hourlyData;
    weatherData.alerts = weatherAlerts;
    weatherData.timezone_offset = timezone_offset;

    // updating data in DOM with selected timeZone format
    updateGeographicalData(currentData, locationData, "initial");
    updateMeteorologicalDataInDOM(currentData);
    updateDaySelectionDataInDOM(dailyData);
    findIndexForChart(currentData.dt);
    updateWeatherAlertsInDOM(weatherAlerts);
};

// updating city and country name, selected time and date, sunrise and sunset time in DOM
const updateGeographicalData = (data, location, status) => {
    const { currentWeather, sevenDaysWeather } = weatherData;
    const currentTime = convertUnixTimeFormat(currentWeather.dt);
    const selectedTime = convertUnixTimeFormat(data.dt);

    // console.log(typeof data.dt)

    const date = `${getDayName(selectedTime.getDay())}, ${selectedTime.toLocaleDateString()}`;

    let sunriseData, sunsetData;
    if (status === "hourly") {
        if (currentTime.getDate() === selectedTime.getDate()) {
            sunriseData = convertUnixTimeFormat(sevenDaysWeather[0].sunrise);
            sunsetData = convertUnixTimeFormat(sevenDaysWeather[0].sunset);
        }
        else {
            sunriseData = convertUnixTimeFormat(sevenDaysWeather[1].sunrise);
            sunsetData = convertUnixTimeFormat(sevenDaysWeather[1].sunset);
        }
    }
    else {
        sunriseData = convertUnixTimeFormat(data.sunrise);
        sunsetData = convertUnixTimeFormat(data.sunset);
    }

    const sunrise = `${correctTimeFormat(sunriseData.getHours())}:${correctTimeFormat(sunriseData.getMinutes())}`;
    const sunset = `${correctTimeFormat(sunsetData.getHours())}:${correctTimeFormat(sunsetData.getMinutes())}`;

    let time = `${correctTimeFormat(currentTime.getHours())}:${correctTimeFormat(currentTime.getMinutes())}`;

    if (status === "hourly") {
        time = `${correctTimeFormat(selectedTime.getHours())}:00`;
    }
    else if (status === "daily") {
        const currentDay = currentTime.getDay();
        const selectedDay = selectedTime.getDay();
        if (currentDay === selectedDay) {
            time = "Today";
        }
        else if (currentDay + 1 === selectedDay || currentDay + 1 === selectedDay + 7) {
            time = "Tomorrow";
        }
        else {
            time = getDayName(selectedDay);
        };
    };

    updateGeographicalDataInDOM(location, time, date, sunrise, sunset, status);
};

const updateGeographicalDataInDOM = (location, time, date, sunrise, sunset, status) => {
    const { city, country } = location;
    const { labels } = chartData;

    displayTime.innerText = time;
    // initial DOM update after fetching data
    if (status === "initial") {
        cityName.innerText = city;
        countryName.innerText = country;
    }
    else if (status === "hourly" && labels[0] <= 2) {
        return;
    };

    displayDate.innerText = date;
    sunriseTime.innerText = sunrise;
    sunsetTime.innerText = sunset;
}

// updating weather data in DOM
const updateMeteorologicalDataInDOM = (data) => {
    let dt, weather, temp, feels_like, pressure, humidity, wind_speed, wind_deg;
    // handling variables values when function is called from different locations
    // current and daily data sets have temp, feels_like, wind_speed and other properties
    // hourly data has main and wind properties
    if (data.wind_speed) {
        ({ dt, weather, temp, feels_like, pressure, humidity, wind_speed, wind_deg } = data);
    }
    else {
        ({ dt, weather, main: { temp, feels_like, pressure, humidity }, wind: { speed: wind_speed, deg: wind_deg } } = data);
    };

    weatherStatusImage.src = updateWeatherStatusImage(weather[0].id, dt);
    mainWeatherStatus.innerText = weather[0].main;
    mainWeatherDescription.innerText = weather[0].description;
    currentTemperature.innerText = updateTemperatureInDOM(temp.day ? temp.day : temp, "main");
    currentTemperatureDecimal.innerText = updateTemperatureInDOM(temp.day ? temp.day : temp, "dec");
    feelsLike.innerText = updateTemperatureInDOM(feels_like.day ? feels_like.day : feels_like, "main");
    feelsLikeDecimal.innerText = updateTemperatureInDOM(feels_like.day ? feels_like.day : feels_like, "dec");
    displayPressure.innerText = pressure;
    displayHumidity.innerText = humidity;
    windSpeed.innerText = handleWindSpeedUnits(wind_speed);
    windDirection.innerText = updateWindDirection(wind_deg);
};

// right section, daily selection DOM elements
const weatherTodayImage = document.getElementById("weatherTodayImage");
const weatherTodayTemperature = document.getElementById("weatherTodayTemperature");
const weatherTodayDay = document.getElementById("weatherTodayDay");
const weatherTomorrowImage = document.getElementById("weatherTomorrowImage");
const weatherTomorrowTemperature = document.getElementById("weatherTomorrowTemperature");
const weatherInTwoDaysImage = document.getElementById("weatherInTwoDaysImage");
const weatherInTwoDaysTemperature = document.getElementById("weatherInTwoDaysTemperature");
const weatherInTwoDaysDay = document.getElementById("weatherInTwoDaysDay");
const weatherInThreeDaysImage = document.getElementById("weatherInThreeDaysImage");
const weatherInThreeDaysTemperature = document.getElementById("weatherInThreeDaysTemperature");
const weatherInThreeDaysDay = document.getElementById("weatherInThreeDaysDay");

// updating daily selection elements in DOM
const updateDaySelectionDataInDOM = (data) => {
    const { units } = appSettingsData;
    const { weather, dt, temp } = weatherData.currentWeather;
    // depending on currentWeatherDisplayed value, first element will be updated with current on daily weather data
    if (currentWeatherDisplayed) {
        weatherTodayImage.src = updateWeatherStatusImage(data[0].weather[0].id, data[0].dt);
        weatherTodayTemperature.innerText = `${data[0].temp.day.toFixed(1)}${updateTemperatureUnitsInDOM(units)}`;
        weatherTodayDay.innerText = "Today";
    }
    else {
        weatherTodayImage.src = updateWeatherStatusImage(weather[0].id, dt);
        weatherTodayTemperature.innerText = `${temp.toFixed(1)}${updateTemperatureUnitsInDOM(units)}`;
        weatherTodayDay.innerText = "Now";
    }

    weatherTomorrowImage.src = updateWeatherStatusImage(data[1].weather[0].id, data[1].dt);
    weatherTomorrowTemperature.innerText = `${data[1].temp.day.toFixed(1)}${updateTemperatureUnitsInDOM(units)}`;
    weatherInTwoDaysImage.src = updateWeatherStatusImage(data[2].weather[0].id, data[2].dt);
    weatherInTwoDaysTemperature.innerText = `${data[2].temp.day.toFixed(1)}${updateTemperatureUnitsInDOM(units)}`;
    weatherInTwoDaysDay.innerText = getDayName(convertUnixTimeFormat(data[2].dt, "day"));
    weatherInThreeDaysImage.src = updateWeatherStatusImage(data[3].weather[0].id, data[3].dt);
    weatherInThreeDaysTemperature.innerText = `${data[3].temp.day.toFixed(1)}${updateTemperatureUnitsInDOM(units)}`;
    weatherInThreeDaysDay.innerText = getDayName(convertUnixTimeFormat(data[3].dt, "day"));
}
//daily selection first element
const weatherToday = document.getElementById("weatherToday");
weatherToday.addEventListener("click", () => updateDaySelectionWeatherDataInDOM(0));

//daily selection second element
const weatherTomorrow = document.getElementById("weatherTomorrow");
weatherTomorrow.addEventListener("click", () => updateDaySelectionWeatherDataInDOM(1));

//daily selection third element
const weatherInTwoDays = document.getElementById("weatherInTwoDays");
weatherInTwoDays.addEventListener("click", () => updateDaySelectionWeatherDataInDOM(2))

//daily selection fourth element
const weatherInThreeDays = document.getElementById("weatherInThreeDays");
weatherInThreeDays.addEventListener("click", () => updateDaySelectionWeatherDataInDOM(3))


const updateDaySelectionWeatherDataInDOM = (index) => {
    if (index === 0) {
        currentWeatherDisplayed = !currentWeatherDisplayed;
        // depending on currentWeatherDisplayed value, updating geographical and meteorologica data
        if (currentWeatherDisplayed) {
            updateGeographicalData(weatherData.currentWeather, {}, "current");
            updateMeteorologicalDataInDOM(weatherData.currentWeather);
        }
        else {
            updateGeographicalData(weatherData.sevenDaysWeather[index], {}, "daily");
            updateMeteorologicalDataInDOM(weatherData.sevenDaysWeather[index]);
        };
    }
    else {
        // setting currentWeatherDisplayed to false to set first element event for current weather
        currentWeatherDisplayed = false;
        updateGeographicalData(weatherData.sevenDaysWeather[index], {}, "daily");
        updateMeteorologicalDataInDOM(weatherData.sevenDaysWeather[index]);
    };
    // updating daily selection elements
    updateDaySelectionDataInDOM(weatherData.sevenDaysWeather);
    // updating chart data
    findIndexForChart(weatherData.sevenDaysWeather[index].dt);
};

// finding first element with selected date in fiveDaysThreeHoursWeather
const findIndexForChart = (date) => {
    const { fiveDaysThreeHoursWeather } = weatherData;
    const selectedDate = convertUnixTimeFormat(date, "date");
    let selectedDayIndex = fiveDaysThreeHoursWeather.findIndex(item => convertUnixTimeFormat(item.dt, "date") === selectedDate);
    // if there is no element with "selectedDate" date, seting index to 0
    selectedDayIndex === -1 ? selectedDayIndex = 0 : "";
    updateChartData(fiveDaysThreeHoursWeather, selectedDayIndex);
    updateThreeHoursWeatherData(fiveDaysThreeHoursWeather, selectedDayIndex)
};
// based on the found index, threeHoursWeather element will be updated 
const updateThreeHoursWeatherData = (data, index) => {
    threeHoursWeather.innerHTML = null;
    for (let i = index; i < index + 8; i++) {
        const hour = convertUnixTimeFormat(data[i].dt, "hours");
        const label = hour < 10 ? `0${hour}` : hour;
        const buttonElement = createNewElement("button", `${label}:00`);
        buttonElement.addEventListener("click", () => {
            updateGeographicalData(weatherData.fiveDaysThreeHoursWeather[i], {}, "hourly");
            updateMeteorologicalDataInDOM(weatherData.fiveDaysThreeHoursWeather[i]);
        });
        threeHoursWeather.appendChild(buttonElement);
    };
};
// based on the found index, chart data will be updated 
const updateChartData = (data, index) => {
    let temp = [];
    let precipitation = [];
    let labels = [];
    for (let i = index; i < index + 8; i++) {
        const temperature = Number((data[i].main.temp).toFixed(1));
        temp = [...temp, temperature];
        precipitation = [...precipitation, data[i].pop];
        const hours = convertUnixTimeFormat(data[i].dt, "hours");
        labels = [...labels, hours];
    };
    chartData = {
        ...chartData,
        temp,
        precipitation,
        labels
    };
    updateChart();
};

let windSpeedUnitSelectionHandler = {
    previousValue: "m/s",
    newValue: "m/s",
};

// windSpeed select DOM element
const selectWindSpeedUnits = document.getElementById("selectWindSpeedUnits");

selectWindSpeedUnits.addEventListener("change", (e) => {
    // updating new value with event value
    windSpeedUnitSelectionHandler.newValue = e.target.value;
    // getting value in windSpeed element when units are changed
    windSpeed.innerText = handleWindSpeedUnits(Number(windSpeed.textContent));
});
// this function is called whenever a new weather data set is rendered
// based on windSpeedUnitSelectionHandler values and selected units in settings, the new wind speed value will be calculated
// windSpeed select element will change new unitsHandler value, the new windSpeed value will be calculated from windSpeed element value
// settings units selection will change both, new and previous unitsHandler value, for metric to m/s, for imperial to mi/h
// when new weather data set is rendered, selected windSpeed unit will stay the same, the new wind speed value will be calculated from speed parameter based on selected units in settings
const handleWindSpeedUnits = (speed) => {
    const { units } = appSettingsData;
    let { previousValue, newValue } = windSpeedUnitSelectionHandler;
    windSpeedUnitSelectionHandler.previousValue = newValue;
    switch (previousValue) {
        case "m/s":
            switch (newValue) {
                case "km/h":
                    return (speed * 3.6).toFixed(1);
                case "mi/h":
                    return (speed * 2.237).toFixed(1);
                case "m/s":
                    switch (units) {
                        case "metric":
                            selectWindSpeedUnits.value = "m/s";
                            return speed.toFixed(1);
                        case "imperial":
                            return (speed / 2.237).toFixed(1);
                    };
            };
        case "km/h":
            switch (newValue) {
                case "m/s":
                    return (speed / 3.6).toFixed(1);;
                case "mi/h":
                    return (speed / 1.609).toFixed(1);
                case "km/h":
                    switch (units) {
                        case "metric":
                            return (speed * 3.6).toFixed(1);
                        case "imperial":
                            return (speed * 1.609).toFixed(1);
                    };
            };
        case "mi/h":
            switch (newValue) {
                case "m/s":
                    return (speed / 2.237).toFixed(1);
                case "km/h":
                    return (speed * 1.609).toFixed(1);
                case "mi/h":
                    switch (units) {
                        case "metric":
                            return (speed * 2.237).toFixed(1);
                        case "imperial":
                            selectWindSpeedUnits.value = "mi/h";
                            return speed.toFixed(1);
                    };
            };
        default:
            return 0.0
    };
};

let slidesDisplayStatus = {
    settings: false,
    weatherAlerts: false
}

// settings DOM elements
const settings = document.getElementById("settings");
const settingsMenuContainer = document.getElementById("settingsMenuContainer");

const metricUnitsSelection = document.getElementById("metricUnitsSelection");
const metricUnitsConfirmation = document.getElementById("metricUnitsConfirmation");

const imperialUnitsSelection = document.getElementById("imperialUnitsSelection");
const imperialUnitsConfirmation = document.getElementById("imperialUnitsConfirmation");

const currentTimeZoneSelection = document.getElementById("currentTimeZoneSelection");
const currentTimeZoneConfirmation = document.getElementById("currentTimeZoneConfirmation");

const localTimeZoneSelection = document.getElementById("localTimeZoneSelection");
const localTimeZoneConfirmation = document.getElementById("localTimeZoneConfirmation");

const displaySettingsInfo = document.getElementById("displaySettingsInfo");
const closeSettingsMenu = document.getElementById("closeSettingsMenu");

const settingsMenuInfo = document.getElementById("settingsMenuInfoContainer");
const hideSettingsInfo = document.getElementById("hideSettingsInfo");

// DOM elements to be animated when setting menu container is displayed
const meinWeatherStatusContainer = document.getElementById("meinWeatherStatusContainer");
const weatherLocationContainer = document.getElementById("weatherLocationContainer");
const sunDataContainer = document.getElementById("sunDataContainer");

let smartphoneResolution;

const handleSettingsContainerDisplayInDOM = (action) => {
    const { weatherAlerts } = slidesDisplayStatus;
    updateElementTransitionStyleProperty("0.8s");
    if (action === "show") {
        slidesDisplayStatus.settings = true;
        if (smartphoneResolution) {
            if (weatherAlerts) {
                handleWeatherAlertsContainerDisplayInDOM("hide");
            };
            settingsMenuContainer.style.right = "0%";
            weatherOverviewContainer.style.left = "100%";
        }
        else {
            settingsMenuContainer.style.right = "80%";
        };
        meinWeatherStatusContainer.style.left = "100%";
        weatherLocationContainer.style.left = "100%";
        sunDataContainer.style.left = "100%";
    }
    else if (action === "hide") {
        slidesDisplayStatus.settings = false;
        if (smartphoneResolution) {
            weatherOverviewContainer.style.left = "0%";
            if (weatherAlerts) {
                updateElementTransitionStyleProperty("none");
            };
        };
        settingsMenuContainer.style.right = "100%";
        meinWeatherStatusContainer.style.left = "0%";
        weatherLocationContainer.style.left = "0%";
        sunDataContainer.style.left = "0%";
        setTimeout(() => {
            handleSettingsInfoContainerDisplayInDOM("hide")
        }, 800);
    }
    else {
        return;
    }
};

settings.addEventListener("click", () => handleSettingsContainerDisplayInDOM("show"));
closeSettingsMenu.addEventListener("click", () => handleSettingsContainerDisplayInDOM("hide"));

const handleSettingsInfoContainerDisplayInDOM = (action) => {
    if (action === "show") {
        settingsMenuInfo.style.left = "0%";
        displaySettingsInfo.style.right = "-95%";
    }
    else if (action === "hide") {
        settingsMenuInfo.style.left = "-100%";
        displaySettingsInfo.style.right = "10px";
    }
    else {
        return;
    };
};

displaySettingsInfo.addEventListener("click", () => handleSettingsInfoContainerDisplayInDOM("show"));
hideSettingsInfo.addEventListener("click", () => handleSettingsInfoContainerDisplayInDOM("hide"));

//settings - units selection event listeners 
metricUnitsSelection.addEventListener("click", () => {
    handleSettingsUnitsSelection("metric");
});

imperialUnitsSelection.addEventListener("click", () => {
    handleSettingsUnitsSelection("imperial");
});

const handleSettingsUnitsSelection = (selectedUnits) => {
    const { units } = appSettingsData;
    // if metric units are selected 
    if (selectedUnits === "metric") {
        // if imperial units were previously selected 
        if (units !== "metric") {
            metricUnitsConfirmation.innerText = "✔";
            imperialUnitsConfirmation.innerText = null;
            appSettingsData.units = "metric";
            windSpeedUnitSelectionHandler.previousValue = "m/s";
            windSpeedUnitSelectionHandler.newValue = "m/s";
            if (locationData) {
                getWeather(locationData);
            };
        }
        // if metric units were previously selected 
        else {
            return;
        };
    }
    // if imperial units are selected 
    else if (selectedUnits === "imperial") {
        // if metric units were previously selected 
        if (units !== "imperial") {
            metricUnitsConfirmation.innerText = null;
            imperialUnitsConfirmation.innerText = "✔";
            appSettingsData.units = "imperial";
            windSpeedUnitSelectionHandler.previousValue = "mi/h";
            windSpeedUnitSelectionHandler.newValue = "mi/h";
            if (locationData) {
                getWeather(locationData)
            };
        }
        // if imperial units were previously selected 
        else {
            return;
        };
    }
    else {
        return;
    }
};

//settings - timeZone selection event listeners 
currentTimeZoneSelection.addEventListener("click", () => {
    handleSettingsTimeZoneSelection("current");
});

localTimeZoneSelection.addEventListener("click", () => {
    handleSettingsTimeZoneSelection("local");
});

const handleSettingsTimeZoneSelection = (selectedTime) => {
    const { timeZone } = appSettingsData;
    const { currentWeather, sevenDaysWeather, fiveDaysThreeHoursWeather, alerts, timezone_offset } = weatherData;
    // if current timeZone is selected
    if (selectedTime === "current") {
        // if local timeZone was previously selected 
        if (timeZone !== "current") {
            currentTimeZoneConfirmation.innerText = "✔";
            localTimeZoneConfirmation.innerText = null;
            appSettingsData.timeZone = "current";
            if (locationData) {
                updateDataWithSelectedTimeZone(currentWeather, sevenDaysWeather, fiveDaysThreeHoursWeather, timezone_offset, alerts);
            };
        }
        // if current timeZone was previously selected 
        else {
            return;
        };
    }
    // if local timeZone is selected
    else if (selectedTime === "local") {
        // if current timeZone was previously selected 
        if (timeZone !== "local") {
            currentTimeZoneConfirmation.innerText = null;
            localTimeZoneConfirmation.innerText = "✔";
            appSettingsData.timeZone = "local";
            if (locationData) {
                updateDataWithSelectedTimeZone(currentWeather, sevenDaysWeather, fiveDaysThreeHoursWeather, timezone_offset, alerts)
            };
        }
        // if local timeZone was previously selected 
        else {
            return;
        };
    }
    else {
        return;
    };
};

// weatherAlerts DOM elements
const weatherAlerts = document.getElementById("weatherAlerts");
const numberOfAlerts = document.getElementById("numberOfAlerts");
const weatherAlertsContainer = document.getElementById("weatherAlertsContainer");
const weatherAlertsContent = document.getElementById("weatherAlertsContentContainer");

// DOM elements to be animated when weatherAlerts container is displayed
const weatherOverviewContainer = document.getElementById("weatherOverviewContainer");
const weatherChartContainer = document.getElementById("weatherChartContainer");
const hideWeatherAlerts = document.getElementById("hideWeatherAlerts");

const handleWeatherAlertsContainerDisplayInDOM = (action) => {
    const { settings } = slidesDisplayStatus;
    updateElementTransitionStyleProperty("0.8s");
    if (action === "show") {
        slidesDisplayStatus.weatherAlerts = true;
        if (smartphoneResolution) {
            if (settings) {
                handleSettingsContainerDisplayInDOM("hide");
            };
            weatherAlertsContainer.style.left = "0%";
            meinWeatherStatusContainer.style.left = "-100%";
            weatherLocationContainer.style.left = "-100%";
            sunDataContainer.style.left = "-100%";
            weatherOverviewContainer.style.left = "-100%";
        }
        else {
            weatherAlertsContainer.style.top = "0%";
            weatherOverviewContainer.style.top = "100%";
            weatherChartContainer.style.top = "100%";
            threeHoursWeather.style.top = "100%";
        };
    }
    else if (action === "hide") {
        slidesDisplayStatus.weatherAlerts = false;
        if (smartphoneResolution) {
            if (settings) {
                updateElementTransitionStyleProperty("none");
            };
            weatherAlertsContainer.style.left = "100%";
            meinWeatherStatusContainer.style.left = "0%";
            weatherLocationContainer.style.left = "0%";
            sunDataContainer.style.left = "0%";
            weatherOverviewContainer.style.left = "0%";
        }
        else {
            weatherAlertsContainer.style.top = "-100%";
            weatherOverviewContainer.style.top = "0%";
            weatherChartContainer.style.top = "0%";
            threeHoursWeather.style.top = "0%";
        };
    }
    else {
        return;
    };
};

weatherAlerts.addEventListener("click", () => handleWeatherAlertsContainerDisplayInDOM("show"));
hideWeatherAlerts.addEventListener("click", () => handleWeatherAlertsContainerDisplayInDOM("hide"));
// event listeners for showing and hiding weatherAlerts elements 

const updateElementTransitionStyleProperty = (value) => {
    weatherOverviewContainer.style.transition = value;
    meinWeatherStatusContainer.style.transition = value;
    weatherLocationContainer.style.transition = value;
    sunDataContainer.style.transition = value;
}

// updating weather alerts in DOM if there is any
const updateWeatherAlertsInDOM = (alerts) => {
    // if the fetch request succeeds after an error the weather alerts container will be hidden and weather details elements will be displayed 
    weatherAlertsContent.innerHTML = null;
    if (smartphoneResolution) {
        handleSettingsContainerDisplayInDOM("hide");
        handleWeatherAlertsContainerDisplayInDOM("hide");
    };
    if (alerts) {
        updateWeatherAlertsContainerStyle("block", alerts)
        alerts.forEach(alert => {
            for (let [property, value] of Object.entries(alert)) {
                const time = typeof value === "number" ? convertUnixTimeFormat(value) : null;
                const fullDate = typeof value === "number" ? time.toLocaleDateString() : null;

                switch (property) {
                    case "sender_name":
                        property = "Issued by";
                        break;
                    case "event":
                        property = "Main";
                        break;
                    case "start":
                        value = `${getDayName(time.getDay())}, ${fullDate}, ${correctTimeFormat(time.getHours())}:${correctTimeFormat(time.getMinutes())}h`;
                        property = "Start"
                        break;
                    case "end":
                        value = `${getDayName(time.getDay())}, ${fullDate}, ${correctTimeFormat(time.getHours())}:${correctTimeFormat(time.getMinutes())}h`;
                        property = "End"
                        break;
                    case "description":
                        property = "Description";
                        break;
                    default:
                        break;
                };

                const headingElement = createNewElement("h2", `${property}:`);
                property === "Description" ? headingElement.style.marginBottom = "0px" : null;
                const spanElement = createNewElement("span", value);
                const brElement = createNewElement("br", null);
                weatherAlertsContent.appendChild(headingElement);
                weatherAlertsContent.appendChild(spanElement);
                weatherAlertsContent.appendChild(brElement);
            }
            const hrElement = createNewElement("hr", null);
            weatherAlertsContent.appendChild(hrElement);
        });
    }
    else {
        updateWeatherAlertsContainerStyle("flex")
        const pElement = createNewElement("p", `no weather alerts for ${locationData.city}`);
        weatherAlertsContent.appendChild(pElement);
    };
};

const updateWeatherAlertsContainerStyle = (status, alerts) => {
    switch (status) {
        case "flex":
            weatherAlertsContent.style.display = "flex";
            weatherAlertsContent.style.textAlign = "center";
            numberOfAlerts.innerText = 0;
            numberOfAlerts.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
            break;
        case "block":
            weatherAlertsContent.style.display = "block";
            weatherAlertsContent.style.textAlign = "justify";
            numberOfAlerts.innerText = alerts.length;
            numberOfAlerts.style.backgroundColor = "rgba(255, 38, 0, 0.8)";
            break;
        default:
            break;
    };
};

// general chart style settings
Chart.defaults.global.defaultFontColor = "white";
Chart.defaults.global.defaultFontFamily = "'Cinzel', serif";
Chart.defaults.global.defaultFontSize = 18;

const updateChart = () => {
    const { units } = appSettingsData;
    const { temp, precipitation, labels } = chartData;
    ctx = document.getElementById('weatherChart').getContext('2d');
    // to prevent chart hover issues with previous data, chart variable is lifted up to window variable level 
    if (window.chart !== undefined) {
        window.chart.destroy();
    }
    window.chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Temperature',
                yAxisID: 'A',
                data: temp,
                fill: false,
                backgroundColor: "#DD4803",
                borderColor: "#DD4803",
                borderWidth: 1,
                pointBorderWidth: 5,
                pointHitRadius: 10
            },
            {
                label: 'Precipitation',
                yAxisID: 'B',
                data: precipitation,
                backgroundColor: "rgba(0,0,256, 0.1)",
                borderColor: "#1F75FF",
                pointBackgroundColor: "#1F75FF",
                lineTension: 0.3,
                borderWidth: 1,
                pointBorderWidth: 5,
                pointHitRadius: 10
            }]
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: true,
                titleAlign: "center",
                titleFontFamily: "'Times New Roman', Times, serif",
                callbacks: {
                    title: (tooltipItem, data) => {
                        return `${data.labels[tooltipItem[0].index]}:00h`;

                    },
                    label: (tooltipItem, data) => {
                        if (tooltipItem.datasetIndex === 0) {
                            return `${data.datasets[0].label}: ${data.datasets[0].data[tooltipItem.index]}${updateTemperatureUnitsInDOM(units)}`
                        }
                        else {
                            return `${data.datasets[1].label}: ${(data.datasets[1].data[tooltipItem.index] * 100).toFixed(0)}%`
                        }
                    }
                },
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: smartphoneResolution ? "bottom" : "top",
                labels: {
                    padding: 10,
                    boxWidth: 20,
                    fontSize: 15
                }
            },
            scales: {
                yAxes: [{
                    id: 'A',
                    type: 'linear',
                    position: 'left',
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                    ticks: {
                        callback: function (value) {
                            return value + "°";
                        },
                        // updating y-axis min and max based on temperature min and max values
                        max: Math.floor(Math.max(...temp)) % 2 === 0 ? Math.floor(Math.max(...temp) + 2) : Math.floor(Math.max(...temp) + 1),
                        min: Math.floor(Math.min(...temp)) % 2 === 0 ? Math.floor(Math.min(...temp) - 2) : Math.floor(Math.min(...temp) - 1),
                        stepSize: 2
                    }
                },
                {
                    id: 'B',
                    type: 'linear',
                    position: 'right',
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                    ticks: {
                        max: 1,
                        min: 0,
                        stepSize: 0.2
                    }
                }],
                xAxes: [{
                    ticks: {
                        callback: (value) => {
                            return correctTimeFormat(value) + "h"
                        },
                        fontSize: 13,
                        fontFamily: "'Times New Roman', Times, serif",
                        display: smartphoneResolution
                    }
                }]
            }
        }
    });
};

window.addEventListener('load', () => {
    updateChart()
    if (window.innerWidth < 1000) {
        smartphoneResolution = true;
    }
    else {
        smartphoneResolution = false;
    };
});


// **util functions**

// updating weather status images in DOM based on weather status id and period of the day
const updateWeatherStatusImage = (id, selectedTime) => {
    const time = convertUnixTimeFormat(selectedTime, "hours");
    switch (true) {
        case id < 300:
            return weatherStatusImages[0];
        case id >= 300 && id <= 501:
            return weatherStatusImages[1];
        case id > 501 && id <= 531:
            return weatherStatusImages[2];
        case id >= 600 && id <= 622:
            return weatherStatusImages[3];
        case id >= 701 && id <= 771:
            return weatherStatusImages[4];
        case id === 781:
            return weatherStatusImages[5];
        case id === 800 && (time > 6 && time < 20):
            return weatherStatusImages[6];
        case id === 800 && (time <= 6 || time >= 20):
            return weatherStatusImages[7];
        case (id === 801 || id === 802) && (time <= 6 || time >= 20):
            return weatherStatusImages[8];
        case (id === 801 || id === 802) && (time > 6 && time < 20):
            return weatherStatusImages[9];
        case id > 802:
            return weatherStatusImages[10];
        default:
            break;
    };
};

// based on degree(meteorological) parameter, wind direction image will be rotated and wind direction will be declared 
const updateWindDirection = (degree) => {
    windDirectionImage.style.transform = `rotate(${degree}deg)`;
    switch (true) {
        case degree >= 337.5 || degree < 22.5:
            return "N";
        case degree >= 22.5 && degree < 67.5:
            return "NE";
        case degree >= 67.5 && degree < 112.5:
            return "E";
        case degree >= 112.5 && degree < 157.5:
            return "SE";
        case degree >= 157.5 && degree < 202.5:
            return "S";
        case degree >= 202.5 && degree < 247.5:
            return "SW";
        case degree >= 247.5 && degree < 292.5:
            return "W";
        case degree >= 292.5 && degree < 337.5:
            return "NW";
        default:
            break;
    };
};

// returning day name based on dayIndex parameter
const getDayName = (dayIndex) => {
    switch (true) {
        case dayIndex === 0:
            return "Sunday";
        case dayIndex === 1:
            return "Monday";
        case dayIndex === 2:
            return "Tuesday";
        case dayIndex === 3:
            return "Wednesday";
        case dayIndex === 4:
            return "Thursday";
        case dayIndex === 5:
            return "Friday";
        case dayIndex === 6:
            return "Saturday";
        default:
            break;
    };
};

// adding 0 in front of hours and minutes which are less than 10
const correctTimeFormat = (time) => {
    if (time < 10) {
        return `0${time}`
    }
    else {
        return time
    };
};

// to handle style of temperature data in weather container(smaller and bigger font), this function separates main and decimal temperature parts
const updateTemperatureInDOM = (temp, type) => {
    const { units } = appSettingsData;
    if (temp) {
        if (type === "main") {
            if (temp >= 0) {
                return `${Math.floor(temp)}.`;
            }
            else {
                return `${Math.ceil(temp)}.`;
            };
        }
        // if decimal part is requested 
        else {
            if (temp >= 0) {
                return `${Math.floor((((temp * 100) - (Math.floor(temp)) * 100) / 10))}${updateTemperatureUnitsInDOM(units)}`;
            }
            else {
                return `${Math.abs(Math.ceil((((temp * 100) - (Math.ceil(temp)) * 100) / 10)))}${updateTemperatureUnitsInDOM(units)}`;
            };
        };
    }
    else {
        return null;
    };
};

const updateTemperatureUnitsInDOM = (units) => {
    if (units === "metric") {
        return "°c"
    }
    else {
        return "°f"
    };
};

const createNewElement = (type, text) => {
    const el = document.createElement(type);
    el.innerText = text;
    return el;
};

const convertUnixTimeFormat = (unix, method) => {
    const time = new Date(unix * 1000);
    switch (method) {
        case "hours":
            return time.getHours();
        case "day":
            return time.getDay();
        case "date":
            return time.getDate();
        default:
            return time;
    };
};

const debounce = (func, delay) => {
    let handler;
    return (e) => {
        if (handler) {
            clearTimeout(handler);
        };
        handler = setTimeout(() => {
            func(e);
        }, delay);
    };
};

const debounce_handler = debounce((e) => getCities(e), 1001);