
//SCORE
let correctAnswers = 0
let incorrectAnswers = 0
let total = 0

//PARAMETERS
let time = 60
let rangeMin = 1
let rangeMax = 10
let addition = false
let subtraction = false
let multiplication = false
let division = false
let exponent = false

let exponentRangeMin = 1;
let exponentRangeMax = 5;

let easy = true
let medium = false
let hard = false
let legend = false

let operators = []
let operator = ""

let interval = 0

let number1 = 0
let number2 = 0
let answer1 = 0
let answer2 = 0
let answer3 = 0
let correctAnswer = 0

let results = new Array(4)
let resultsMix = new Array(4)


document.getElementById('startGameButton').addEventListener('click', startGame)

document.getElementById('time').addEventListener('change', () => {
    time = document.getElementById('time').value;
    document.getElementById('timer').innerHTML = `Timer: ${time}s`;
    let x = -2.3;
    console.log(Math.pow(x, 3.4))
})

document.getElementById('rangeMin').addEventListener('change', () => {
    rangeMin = Number(document.getElementById('rangeMin').value);
})

document.getElementById('rangeMax').addEventListener('change', () => {
    rangeMax = Number(document.getElementById('rangeMax').value);
})

document.getElementById('exponentRangeMin').addEventListener('change', () => {
    exponentRangeMin = Number(document.getElementById('exponentRangeMin').value);
})

document.getElementById('exponentRangeMax').addEventListener('change', () => {
    exponentRangeMax = Number(document.getElementById('exponentRangeMax').value);
})

document.getElementById('easy').checked = true;

document.getElementById('addition').addEventListener('change', () => {
    addition = document.getElementById('addition').checked;
})

document.getElementById('subtraction').addEventListener('change', () => {
    subtraction = document.getElementById('subtraction').checked;
})

document.getElementById('multiplication').addEventListener('change', () => {
    multiplication = document.getElementById('multiplication').checked;
})

document.getElementById('division').addEventListener('change', () => {
    division = document.getElementById('division').checked;
})

document.getElementById('exponent').addEventListener('change', () => {
    exponent = document.getElementById('exponent').checked;
    if (exponent) {

        if (hard || legend) {
            document.getElementById('exponentRange').style.display = "block";
        }
        else {
            document.getElementById('exponentRange').style.display = "none";
        }
    }
    else {
        document.getElementById('exponentRange').style.display = "none";
    }
});

document.getElementById('easy').addEventListener('change', () => {
    easy = true;
    document.getElementById('easy').checked = easy;
    document.getElementById('medium').checked = false;
    document.getElementById('hard').checked = false;
    document.getElementById('legend').checked = false;
    document.getElementById('exponent').disabled = true;
    document.getElementById('exponent').checked = false
    exponent = false;
    document.getElementById('exponentRange').style.display = "none";
    medium = false;
    hard = false;
    legend = false;
    let body = document.getElementsByTagName('body')[0];
    body.style.backgroundImage = 'url(./Photos/easy.jpg)';
    body.style.backgroundSize = "cover";
})

document.getElementById('medium').addEventListener('change', () => {
    medium = true;
    document.getElementById('medium').checked = medium;
    document.getElementById('easy').checked = false;
    document.getElementById('hard').checked = false;
    document.getElementById('legend').checked = false;
    easy = false;
    hard = false;
    legend = false;
    document.getElementById('exponent').disabled = false
    if (exponent) {
        document.getElementById('exponentRange').style.display = "none";
    }
    let body = document.getElementsByTagName('body')[0]
    body.style.backgroundImage = 'url(./Photos/medium.jpg)'
    body.style.backgroundSize = "cover"
})

document.getElementById('hard').addEventListener('change', () => {
    hard = true;
    document.getElementById('hard').checked = hard;
    document.getElementById('easy').checked = false;
    document.getElementById('medium').checked = false;
    document.getElementById('legend').checked = false;
    easy = false;
    medium = false;
    legend = false;
    document.getElementById('exponent').disabled = false
    if (exponent) {
        document.getElementById('exponentRange').style.display = "block";
    }
    let body = document.getElementsByTagName('body')[0];
    body.style.backgroundImage = 'url(./Photos/hard.jpg)';
    body.style.backgroundSize = "cover";
})

document.getElementById('legend').addEventListener('change', () => {
    legend = true;
    document.getElementById('legend').checked = legend;
    document.getElementById('easy').checked = false;
    document.getElementById('medium').checked = false;
    document.getElementById('hard').checked = false;
    document.getElementById('exponent').disabled = false;
    easy = false;
    medium = false;
    hard = false;
    document.getElementById('exponent').disabled = false
    if (exponent) {
        document.getElementById('exponentRange').style.display = "block";
    }
    let body = document.getElementsByTagName('body')[0];
    body.style.background = 'url(./Photos/legend.JPEG)';
    body.style.backgroundSize = "cover";
})

document.getElementById('renderInfo').addEventListener('click', () => {
    document.getElementById('infoContainer').classList.add('showInfo')
    document.getElementById('infoContainer').classList.remove('hideInfo')
})

document.getElementById('closeInfo').addEventListener('click', () => {
    document.getElementById('infoContainer').classList.add('hideInfo')
    document.getElementById('infoContainer').classList.remove('showInfo')
})


function startGame() {

    setParameters()
    handleInputErrors();
}

function setParameters() {
    if (addition) {
        operators.push("+")
    }
    if (subtraction) {
        operators.push("-")
    }
    if (multiplication) {
        operators.push("*")
    }
    if (division) {
        operators.push("/")
    }
    if (exponent) {
        operators.push("^", "√")
    }
}

function checkAnswer1() {
    playGame(resultsMix[0])
}
function checkAnswer2() {
    playGame(resultsMix[1])
}
function checkAnswer3() {
    playGame(resultsMix[2])
}
function checkAnswer4() {
    playGame(resultsMix[3])
}


function handleInputErrors() {
    if (operators.length === 0) {
        document.getElementById('message').style.opacity = "1";
        document.getElementById('message').style.zIndex = "1";
        document.getElementById('message').innerText = "Mathematical operation not selected!";
        return;
    }
    else if (rangeMin > rangeMax) {
        document.getElementById('message').style.opacity = "1";
        document.getElementById('message').style.zIndex = "1";
        document.getElementById('message').innerHTML = `Invalid range input! <br><br>
        "From" number cannot be greater than "To" number.`;
        return;
    }
    else if (exponentRangeMin > exponentRangeMax) {
        document.getElementById('message').style.opacity = "1";
        document.getElementById('message').style.zIndex = "1";
        document.getElementById('message').innerHTML = `Invalid exponent range input! <br><br>
        "From" number cannot be greater than "To" number.`;
        return;
    }
    else if (2 > rangeMax && exponent) {
        document.getElementById('message').style.opacity = "1";
        document.getElementById('message').style.zIndex = "1";
        document.getElementById('message').innerText = 'When "y√x and x^y" operation selected, range "To" number must be >= 2.';
        return;
    }

    else {
        document.getElementById('message').style.opacity = "0";
        document.getElementById('message').style.zIndex = "-1";
        document.getElementById('message').innerText = "";
        interval = setInterval(gameTimer, 1000);

        document.getElementById('time').disabled = true;
        document.getElementById('rangeMin').disabled = true;
        document.getElementById('rangeMax').disabled = true;
        document.getElementById('exponentRangeMin').disabled = true;
        document.getElementById('exponentRangeMax').disabled = true;

        document.getElementById('startGameButton').innerHTML = "RESET"

        document.getElementById('answer1').addEventListener('click', checkAnswer1)
        document.getElementById('answer2').addEventListener('click', checkAnswer2)
        document.getElementById('answer3').addEventListener('click', checkAnswer3)
        document.getElementById('answer4').addEventListener('click', checkAnswer4)

        document.getElementById('startGameButton').removeEventListener('click', startGame)
        document.getElementById('startGameButton').addEventListener('click', playAgain);

        renderNewTask()
    }
}

function gameTimer() {

    time--

    if (time < 10) {
        document.getElementById('timer').innerHTML = `Timer: 0${time}s`
    }
    else {
        document.getElementById('timer').innerHTML = `Timer: ${time}s`
    }

    if (time === 0) {
        clearInterval(interval);
        document.getElementById('startGameButton').innerText = "PLAY AGAIN"
        document.getElementById('message').style.opacity = "1"
        document.getElementById('message').style.zIndex = "1"
        document.getElementById('message').innerHTML = `GAME OVER <br>  <br>
                                                    Correct Answers: ${correctAnswers} <br>
                                                    Incorrect Answers: ${incorrectAnswers} <br>
                                                    TOTAL: ${total}`
    }
}


function playAgain() {

    clearInterval(interval);

    correctAnswers = 0;
    incorrectAnswers = 0;
    total = 0;
    time = document.getElementById('time').value;
    operators = [];

    document.getElementById('time').disabled = false;
    document.getElementById('rangeMin').disabled = false;
    document.getElementById('rangeMax').disabled = false;
    document.getElementById('exponentRangeMin').disabled = false;
    document.getElementById('exponentRangeMax').disabled = false;

    document.getElementById('firstNumber').innerText = '';
    document.getElementById('operator').innerText = '';
    document.getElementById('secondNumber').innerText = '';
    document.getElementById('answer1').innerText = '';
    document.getElementById('answer2').innerText = '';
    document.getElementById('answer3').innerText = '';
    document.getElementById('answer4').innerText = '';

    document.getElementById('startGameButton').innerText = "START GAME";
    document.getElementById('timer').innerHTML = `Timer: ${time}s`;
    document.getElementById('score').innerText = `Score: 0`;
    document.getElementById('message').style.opacity = "0";
    document.getElementById('message').style.zIndex = "-1";

    document.getElementById('answer1').removeEventListener('click', checkAnswer1);
    document.getElementById('answer2').removeEventListener('click', checkAnswer2);
    document.getElementById('answer3').removeEventListener('click', checkAnswer3);
    document.getElementById('answer4').removeEventListener('click', checkAnswer4);

    document.getElementById('startGameButton').removeEventListener('click', playAgain);
    document.getElementById('startGameButton').addEventListener('click', startGame);
}

function playGame(answer) {

    if (answer === correctAnswer) {

        renderNewTask()

        document.getElementById('answerStatus').innerHTML = "CORRECT"
        document.getElementById('answerStatus').classList.add('correctAnswer');

        setTimeout(() => {
            document.getElementById('answerStatus').classList.remove('correctAnswer')
        }, 1000)

        correctAnswers++
        total = correctAnswers - incorrectAnswers

        document.getElementById('score').innerHTML = `Score: ${total}`
    }

    else {

        document.getElementById('answerStatus').innerHTML = "INCORRECT";
        document.getElementById('answerStatus').classList.add('wrongAnswer');

        setTimeout(() => {
            document.getElementById('answerStatus').classList.remove('wrongAnswer');
        }, 1000);

        incorrectAnswers++;
        total = correctAnswers - incorrectAnswers;
        document.getElementById('score').innerHTML = `Score: ${total}`;
    }
}

function renderNewTask() {

    operators.sort(() => { return 0.5 - Math.random() });
    operator = operators[0];

    if (operator === "√") {
        document.getElementById('operator').style.fontSize = "70px"
    }
    else if (operator === "^") {
        document.getElementById('operator').style.fontSize = "50px"
    }
    else {
        document.getElementById('operator').style.fontSize = "40px"
    }



    if (legend) {
        playLegendGame();
        document.getElementById('firstNumber').style.fontSize = "40px"
        document.getElementById('secondNumber').style.fontSize = "40px"
    }
    else if (hard) {
        playHardGame();
        document.getElementById('firstNumber').style.fontSize = "45px"
        document.getElementById('secondNumber').style.fontSize = "45px"
    }
    else if (medium) {
        playMediumGame();
        document.getElementById('firstNumber').style.fontSize = "70px"
        document.getElementById('secondNumber').style.fontSize = "70px"
    }
    else {
        playEasyGame();
        document.getElementById('firstNumber').style.fontSize = "75px"
        document.getElementById('secondNumber').style.fontSize = "75px"
    }

    document.getElementById('firstNumber').innerHTML = number1;
    document.getElementById('operator').innerHTML = operator;
    document.getElementById('secondNumber').innerHTML = number2;
    document.getElementById('answer1').innerHTML = resultsMix[0];
    document.getElementById('answer2').innerHTML = resultsMix[1];
    document.getElementById('answer3').innerHTML = resultsMix[2];
    document.getElementById('answer4').innerHTML = resultsMix[3];
}


function playEasyGame() {

    number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
    number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)

    if (operator === "+") {
        correctAnswer = number1 + number2
    }
    else if (operator === "-") {
        while (number2 > number1) {
            number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
            number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        }
        correctAnswer = number1 - number2
    }
    else if (operator === "*") {
        correctAnswer = number1 * number2
    }
    else {
        while (number1 % number2 !== 0 || number2 === 0) {
            number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
            number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        }
        correctAnswer = number1 / number2
    }
    answer1 = correctAnswer + 3
    answer2 = correctAnswer - 3
    answer3 = correctAnswer + 6

    results = [answer1, answer2, answer3, correctAnswer]
    resultsMix = results.sort(() => { return 0.5 - Math.random() });
}


function playMediumGame() {

    number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
    number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)

    if (operator === "+") {
        correctAnswer = number1 + number2
    }
    else if (operator === "-") {
        correctAnswer = number1 - number2
    }
    else if (operator === "*") {
        correctAnswer = number1 * number2
    }
    else if (operator === "/") {
        while (number1 % number2 !== 0 || number2 === 0) {
            number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
            number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        }
        correctAnswer = number1 / number2
    }
    else if (operator === "^") {
        number2 = 2
        correctAnswer = number1 * number1
    }
    else if (operator === "√") {
        number1 = operator;
        operator = ''
        number2 = number2 * number2
        while (number2 === 0) {
            number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
            number2 = number2 * number2
        }
        correctAnswer = Math.sqrt(number2)
    }

    answer1 = correctAnswer + 1
    answer2 = correctAnswer - 1
    answer3 = correctAnswer - 2

    results = [answer1, answer2, answer3, correctAnswer]
    resultsMix = results.sort(() => { return 0.5 - Math.random() });
}



function playHardGame() {

    number1 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(1)
    number2 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(1)

    let number1Fixed = Number(number1)
    let number2Fixed = Number(number2)

    if (operator === "+") {
        let correctAnswerNumber = ((number1Fixed * 100) + (number2Fixed * 100)) / 100
        correctAnswer = (correctAnswerNumber).toFixed(1)
        let answer1Number = (correctAnswerNumber + 1)
        answer1 = (answer1Number).toFixed(1)
        let answer2Number = (correctAnswerNumber - 1)
        answer2 = (answer2Number).toFixed(1)
        let answer3Number = (correctAnswerNumber - 2)
        answer3 = (answer3Number).toFixed(1)
    }
    else if (operator === "-") {
        let correctAnswerNumber = ((number1Fixed * 100) - (number2Fixed * 100)) / 100
        correctAnswer = (correctAnswerNumber).toFixed(1)
        let answer1Number = (correctAnswerNumber + 1)
        answer1 = (answer1Number).toFixed(1)
        let answer2Number = (correctAnswerNumber - 1)
        answer2 = (answer2Number).toFixed(1)
        let answer3Number = (correctAnswerNumber + 2)
        answer3 = (answer3Number).toFixed(1)
    }
    else if (operator === "*") {
        let correctAnswerNumber = ((number1Fixed * 100) * (number2Fixed * 100)) / 10000
        correctAnswer = (correctAnswerNumber).toFixed(2)
        let answer1Number = (correctAnswerNumber + 0.1)
        answer1 = (answer1Number).toFixed(2)
        let answer2Number = (correctAnswerNumber - 0.1)
        answer2 = (answer2Number).toFixed(2)
        let answer3Number = (correctAnswerNumber - 0.2)
        answer3 = (answer3Number).toFixed(2)
    }
    else if (operator === "/") {
        let correctAnswerNumber = ((number1Fixed * 100) / (number2Fixed * 100))
        correctAnswer = (correctAnswerNumber).toFixed(2)
        let answer1Number = (correctAnswerNumber + 0.1)
        answer1 = (answer1Number).toFixed(2)
        let answer2Number = (correctAnswerNumber - 0.1)
        answer2 = (answer2Number).toFixed(2)
        let answer3Number = (correctAnswerNumber + 0.2)
        answer3 = (answer3Number).toFixed(2)
    }

    else if (operator === "^") {
        number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        number2 = Math.floor(Math.random() * (exponentRangeMax - exponentRangeMin) + exponentRangeMin)

        while (number1 === 0) {
            number1 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        }

        let number1Fixed = Number(number1)
        let number2Fixed = Number(number2)

        if (number2Fixed < -1) {
            let correctAnswerNumber = Math.pow(number1Fixed, number2Fixed)
            correctAnswer = (correctAnswerNumber).toFixed(8)

            let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
            answer1 = (answer1Number).toFixed(8)
            let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
            answer2 = (answer2Number).toFixed(8)
            let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
            answer3 = (answer3Number).toFixed(8)
        }
        else {
            let correctAnswerNumber = Math.pow(number1Fixed, number2Fixed)
            correctAnswer = (correctAnswerNumber).toFixed(2)

            let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
            answer3 = (answer3Number).toFixed(2)
        }
    }

    else if (operator === "√") {
        number1 = Math.floor(Math.random() * (exponentRangeMax - exponentRangeMin) + exponentRangeMin)
        number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        while (number1 === 0 || number2 <= 0) {
            number1 = Math.floor(Math.random() * (exponentRangeMax - exponentRangeMin) + exponentRangeMin)
            number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
        }
        let correctAnswerNumber = Math.pow(number2, (1 / number1))
        correctAnswer = (correctAnswerNumber).toFixed(2)
        let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
        answer1 = (answer1Number).toFixed(2)
        let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
        answer2 = (answer2Number).toFixed(2)
        let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
        answer3 = (answer3Number).toFixed(2)
    }

    results = [answer1, answer2, answer3, correctAnswer]
    resultsMix = results.sort(() => { return 0.5 - Math.random() });
}


function playLegendGame() {

    number1 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(2)
    number2 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(2)

    let number1Fixed = Number(number1)
    let number2Fixed = Number(number2)

    if (operator === "+") {
        let correctAnswerNumber = ((number1Fixed * 100) + (number2Fixed * 100)) / 100
        correctAnswer = (correctAnswerNumber).toFixed(2)
        let answer1Number = (correctAnswerNumber + 0.1)
        answer1 = (answer1Number).toFixed(2)
        let answer2Number = (correctAnswerNumber - 0.1)
        answer2 = (answer2Number).toFixed(2)
        let answer3Number = (correctAnswerNumber - 0.2)
        answer3 = (answer3Number).toFixed(2)
    }
    else if (operator === "-") {
        let correctAnswerNumber = ((number1Fixed * 100) - (number2Fixed * 100)) / 100
        correctAnswer = (correctAnswerNumber).toFixed(2)
        let answer1Number = (correctAnswerNumber + 0.1)
        answer1 = (answer1Number).toFixed(2)
        let answer2Number = (correctAnswerNumber - 0.1)
        answer2 = (answer2Number).toFixed(2)
        let answer3Number = (correctAnswerNumber + 0.2)
        answer3 = (answer3Number).toFixed(2)
    }
    else if (operator === "*") {
        let correctAnswerNumber = ((number1Fixed * 100) * (number2Fixed * 100)) / 10000
        correctAnswer = (correctAnswerNumber).toFixed(3)
        let answer1Number = (correctAnswerNumber + 0.01)
        answer1 = (answer1Number).toFixed(3)
        let answer2Number = (correctAnswerNumber - 0.01)
        answer2 = (answer2Number).toFixed(3)
        let answer3Number = (correctAnswerNumber - 0.02)
        answer3 = (answer3Number).toFixed(3)
    }
    else if (operator === "/") {
        let correctAnswerNumber = ((number1Fixed * 100) / (number2Fixed * 100))
        correctAnswer = (correctAnswerNumber).toFixed(3)
        let answer1Number = (correctAnswerNumber + 0.01)
        answer1 = (answer1Number).toFixed(3)
        let answer2Number = (correctAnswerNumber - 0.01)
        answer2 = (answer2Number).toFixed(3)
        let answer3Number = (correctAnswerNumber + 0.02)
        answer3 = (answer3Number).toFixed(3)
    }
    else if (operator === "^") {
        number1 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(1)
        number2 = (Math.random() * (exponentRangeMax - exponentRangeMin) + exponentRangeMin).toFixed(1)

        if (number1 >= 0) {
            while (number1 === 0) {
                number1 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(1)
            }
            let number1Fixed = Number(number1)
            let number2Fixed = Number(number2)

            if (number2Fixed < -1) {
                let correctAnswerNumber = Math.pow(number1Fixed, number2Fixed)
                correctAnswer = (correctAnswerNumber).toFixed(8)

                let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                answer1 = (answer1Number).toFixed(8)
                let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                answer2 = (answer2Number).toFixed(8)
                let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                answer3 = (answer3Number).toFixed(8)
            }
            else {
                let correctAnswerNumber = Math.pow(number1Fixed, number2Fixed)
                correctAnswer = (correctAnswerNumber).toFixed(2)

                let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                answer1 = (answer1Number).toFixed(2)
                let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                answer2 = (answer2Number).toFixed(2)
                let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                answer3 = (answer3Number).toFixed(2)
            }
        }
        else {
            while ((number2 * 10) % 2 !== 0) {   //to prevent (-x)^y - y to be odd number
                number2 = Math.floor(Math.random() * (rangeMax - rangeMin) + rangeMin)
            }
            let number1Fixed = Number(number1)
            let number2Fixed = Number(number2)

            if (number2Fixed < -1) {
                let correctAnswerNumber = Math.pow(number1Fixed, number2Fixed)
                correctAnswer = (correctAnswerNumber).toFixed(8)

                let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                answer1 = (answer1Number).toFixed(8)
                let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                answer2 = (answer2Number).toFixed(8)
                let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                answer3 = (answer3Number).toFixed(8)
            }
            else {
                let correctAnswerNumber = Math.pow(number1Fixed, number2Fixed)
                correctAnswer = (correctAnswerNumber).toFixed(2)

                let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                answer1 = (answer1Number).toFixed(2)
                let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                answer2 = (answer2Number).toFixed(2)
                let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                answer3 = (answer3Number).toFixed(2)
            }
        }
    }

    else if (operator === "√") {
        number1 = (Math.random() * (exponentRangeMax - exponentRangeMin) + exponentRangeMin).toFixed(1)
        number2 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(1)

        while (number1 === 0 || number2 <= 0) {
            number1 = (Math.random() * (exponentRangeMax - exponentRangeMin) + exponentRangeMin).toFixed(1)
            number2 = (Math.random() * (rangeMax - rangeMin) + rangeMin).toFixed(1)
        }
        let number1Fixed = Number(number1)
        let number2Fixed = Number(number2)

        let correctAnswerNumber = Math.pow(number2Fixed, (1 / number1Fixed))
        correctAnswer = (correctAnswerNumber).toFixed(2)
        let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
        answer1 = (answer1Number).toFixed(2)
        let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
        answer2 = (answer2Number).toFixed(2)
        let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
        answer3 = (answer3Number).toFixed(2)
    }

    results = [answer1, answer2, answer3, correctAnswer]
    resultsMix = results.sort(() => { return 0.5 - Math.random() });
}