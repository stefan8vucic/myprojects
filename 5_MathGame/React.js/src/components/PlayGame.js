import React, { Component } from "react"
import '../App.css'
import Header from './Header';
import Info from './Info';

class PlayGame extends Component {

    state = {
        level: "easy",
        operations: [],
        operator: "",
        time: 0,
        taskNumber1: "",
        taskNumber2: "",
        answers: [],
        correctAnswer: "",
        buttonStatus: "START GAME",
        scoreTotal: 0,
        scoreCorrectAnswers: 0,
        scoreWrongAnswers: 0,
        messageZindex: -1,
        messageText: [],
        answerStatus: "",
        taskFontSize: "75px",
        operatorFontSize: "40px"
    }

    timer = "";
    rangeMin = 1
    rangeMax = 10;
    exponentRangeMin = 1;
    exponentRangeMax = 5;


    startGame = () => {

        this.props.updateOperations();

        if (this.props.operations.length === 0) {
            this.setState({
                messageZindex: 1,
                messageText: ["Mathematical operation not selected!"]
            })
        }
        else if (this.props.parameters.rangeMin > this.props.parameters.rangeMax) {
            this.setState({
                messageZindex: 1,
                messageText: ["Invalid range input!",
                    '"From" number cannot be greater than "To" number.']
            })
        }
        else if (this.props.parameters.exponentRangeMin > this.props.parameters.exponentRangeMax) {
            this.setState({
                messageZindex: 1,
                messageText: ["Invalid exponent range input!",
                    '"From" number cannot be greater than "To" number.']
            })
        }
        else if (2 > this.props.parameters.rangeMax && this.props.parameters.exponent) {
            this.setState({
                messageZindex: 1,
                messageText: ["",'When "y√x and x^y" operation selected, range "To" number must be >= 2.']
            })
        }
        else {
            let operationMix = this.props.operations;
            operationMix.sort(() => { return 0.5 - Math.random() });
            let operator = operationMix[0];

            this.rangeMin = Number(this.props.parameters.rangeMin)
            this.rangeMax = Number(this.props.parameters.rangeMax)
            this.exponentRangeMin = Number(this.props.parameters.exponentRangeMin)
            this.exponentRangeMax = Number(this.props.parameters.exponentRangeMax)


            this.setState({
                messageZindex: -1,
                level: this.props.parameters.level,
                operations: operationMix,
                operator: operator,
                time: this.props.time,
                buttonStatus: "RESTART GAME"
            })

            this.updateTasksFont();
            this.renderTask(operator)
            this.timer = setInterval(this.gameTimer, 1000)
        }
    }

    updateTasksFont = () => {
        if(this.props.parameters.level === "easy"){
            this.setState({
                taskFontSize: "75px"
            })
        }
        else if(this.props.parameters.level === "medium"){
            this.setState({
                taskFontSize: "70px"
            })
        }
        else if(this.props.parameters.level === "hard"){
            this.setState({
                taskFontSize: "45px"
            })
        }
        else if(this.props.parameters.level === "legend"){
            this.setState({
                taskFontSize: "40px"
            })
        }
    }

    playGame = (answer) => {

        if (answer === this.state.correctAnswer) {
            let operations = this.state.operations;
            operations.sort(() => { return 0.5 - Math.random() });
            let operator = operations[0];
            this.setState({
                operator: operator
            })
            this.renderTask(operator)
            this.setState({
                answerStatus: "correctAnswer",
                scoreCorrectAnswers: this.state.scoreCorrectAnswers + 1,
                scoreTotal: this.state.scoreTotal + 1,
            })
        }
        else {
            this.setState({
                answerStatus: "wrongAnswer",
                scoreWrongAnswers: this.state.scoreWrongAnswers + 1,
                scoreTotal: this.state.scoreTotal - 1
            })
        }

        setTimeout(() => this.setState({
            answerStatus: ""
        }), 500)

    }

    renderTask = (operator) => {

        if(operator === "√"){
            this.setState({
                operatorFontSize: "70px"
            })
        }
        else if(operator === "^"){
            this.setState({
                operatorFontSize: "50px"
            })
        }
        else{
            this.setState({
                operatorFontSize: "40px"
            })
        }

        if (this.props.parameters.level === "easy") {
            this.playEasyGame(operator);
        }
        else if (this.props.parameters.level === "medium") {
            this.playMediumGame(operator);
        }
        else if (this.props.parameters.level === "hard") {
            this.playHardGame(operator);
        }
        else if (this.props.parameters.level === "legend") {
            this.playLegendGame(operator);
        }
    }

    playEasyGame = (operator) => {

        let taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin);
        let taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin);
        let correctAnswer = 0;
        let answers = [];
        if (operator === "+") {
            correctAnswer = taskNumber1 + taskNumber2
        }
        else if (operator === "-") {
            while (taskNumber2 > taskNumber1) {
                taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
                taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
            }
            correctAnswer = taskNumber1 - taskNumber2
        }
        else if (operator === "*") {
            correctAnswer = taskNumber1 * taskNumber2
        }
        else {
            while (taskNumber1 % taskNumber2 !== 0 || taskNumber2 === 0) {
                taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
                taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
            }
            correctAnswer = taskNumber1 / taskNumber2
        }
        let answer1 = correctAnswer + 3
        let answer2 = correctAnswer - 3
        let answer3 = correctAnswer + 6

        answers.push(answer1, answer2, answer3, correctAnswer);

        answers.sort(() => { return 0.5 - Math.random() });

        this.setState({
            taskNumber1: taskNumber1,
            taskNumber2: taskNumber2,
            answers: answers,
            correctAnswer: correctAnswer
        })
    }

    playMediumGame = (operator) => {

        let taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
        let taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
        let correctAnswer = 0;
        let answers = [];

        if (operator === "+") {
            correctAnswer = taskNumber1 + taskNumber2
        }
        else if (operator === "-") {
            correctAnswer = taskNumber1 - taskNumber2
        }
        else if (operator === "*") {
            correctAnswer = taskNumber1 * taskNumber2
        }
        else if (operator === "/") {
            while (taskNumber1 % taskNumber2 !== 0 || taskNumber2 === 0) {
                taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
                taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
            }
            correctAnswer = taskNumber1 / taskNumber2
        }
        else if (operator === "^") {
            taskNumber2 = 2
            correctAnswer = taskNumber1 * taskNumber1
        }
        else if (operator === "√") {
            this.setState({
                operator: ""
            })
            taskNumber1 = operator;
            operator = ''
            taskNumber2 = taskNumber2 * taskNumber2
            while (taskNumber2 === 0) {
                taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
                taskNumber2 = taskNumber2 * taskNumber2
            }
            correctAnswer = Math.sqrt(taskNumber2)
        }

        let answer1 = correctAnswer + 1
        let answer2 = correctAnswer - 1
        let answer3 = correctAnswer - 2

        answers.push(answer1, answer2, answer3, correctAnswer);

        answers.sort(() => { return 0.5 - Math.random() });

        this.setState({
            taskNumber1: taskNumber1,
            taskNumber2: taskNumber2,
            answers: answers,
            correctAnswer: correctAnswer
        })
    }

    playHardGame = (operator) => {

        let taskNumber1 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(1)
        let taskNumber2 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(1)
        let correctAnswer = 0;
        let answers = [];;
        let answer1 = 0;
        let answer2 = 0;
        let answer3 = 0;

        let taskNumber1Fixed = Number(taskNumber1)
        let taskNumber2Fixed = Number(taskNumber2)

        if (operator === "+") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) + (taskNumber2Fixed * 100)) / 100
            correctAnswer = (correctAnswerNumber).toFixed(1)
            let answer1Number = (correctAnswerNumber + 1)
            answer1 = (answer1Number).toFixed(1)
            let answer2Number = (correctAnswerNumber - 1)
            answer2 = (answer2Number).toFixed(1)
            let answer3Number = (correctAnswerNumber - 2)
            answer3 = (answer3Number).toFixed(1)
        }
        else if (operator === "-") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) - (taskNumber2Fixed * 100)) / 100
            correctAnswer = (correctAnswerNumber).toFixed(1)
            let answer1Number = (correctAnswerNumber + 1)
            answer1 = (answer1Number).toFixed(1)
            let answer2Number = (correctAnswerNumber - 1)
            answer2 = (answer2Number).toFixed(1)
            let answer3Number = (correctAnswerNumber + 2)
            answer3 = (answer3Number).toFixed(1)
        }
        else if (operator === "*") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) * (taskNumber2Fixed * 100)) / 10000
            correctAnswer = (correctAnswerNumber).toFixed(2)
            let answer1Number = (correctAnswerNumber + 0.1)
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - 0.1)
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber - 0.2)
            answer3 = (answer3Number).toFixed(2)
        }
        else if (operator === "/") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) / (taskNumber2Fixed * 100))
            correctAnswer = (correctAnswerNumber).toFixed(2)
            let answer1Number = (correctAnswerNumber + 0.1)
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - 0.1)
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber + 0.2)
            answer3 = (answer3Number).toFixed(2)
        }

        else if (operator === "^") {
            taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin);
            taskNumber2 = Math.floor(Math.random() * (this.exponentRangeMax - this.exponentRangeMin) + this.exponentRangeMin);

            while (taskNumber1 === 0) {
                taskNumber1 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin);
            }
            let taskNumber1Fixed = Number(taskNumber1)
            let taskNumber2Fixed = Number(taskNumber2)

            if (taskNumber2Fixed < -1) {
                let correctAnswerNumber = Math.pow(taskNumber1Fixed, taskNumber2Fixed)
                correctAnswer = (correctAnswerNumber).toFixed(8)

                let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                answer1 = (answer1Number).toFixed(8)
                let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                answer2 = (answer2Number).toFixed(8)
                let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                answer3 = (answer3Number).toFixed(8)
            }
            else {
                let correctAnswerNumber = Math.pow(taskNumber1Fixed, taskNumber2Fixed)
                correctAnswer = (correctAnswerNumber).toFixed(2)

                let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                answer1 = (answer1Number).toFixed(2)
                let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                answer2 = (answer2Number).toFixed(2)
                let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                answer3 = (answer3Number).toFixed(2)
            }
        }

        else if (operator === "√") {
            taskNumber1 = Math.floor(Math.random() * (this.exponentRangeMax - this.exponentRangeMin) + this.exponentRangeMin)
            taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
            while (taskNumber1 === 0 || taskNumber2 <= 0) {
                taskNumber1 = Math.floor(Math.random() * (this.exponentRangeMax - this.exponentRangeMin) + this.exponentRangeMin)
                taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
            }
            let correctAnswerNumber = Math.pow(taskNumber2, (1 / taskNumber1))
            correctAnswer = (correctAnswerNumber).toFixed(2)
            let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber + (correctAnswerNumber / 40))
            answer3 = (answer3Number).toFixed(2)
        }
        answers.push(answer1, answer2, answer3, correctAnswer);

        answers.sort(() => { return 0.5 - Math.random() });

        this.setState({
            taskNumber1: taskNumber1,
            taskNumber2: taskNumber2,
            answers: answers,
            correctAnswer: correctAnswer
        })
    }

    playLegendGame = (operator) => {
        let taskNumber1 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(2)
        let taskNumber2 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(2)
        let correctAnswer = 0;
        let answers = [];
        let answer1 = 0;
        let answer2 = 0;
        let answer3 = 0;

        let taskNumber1Fixed = Number(taskNumber1)
        let taskNumber2Fixed = Number(taskNumber2)

        if (operator === "+") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) + (taskNumber2Fixed * 100)) / 100
            correctAnswer = (correctAnswerNumber).toFixed(2)
            let answer1Number = (correctAnswerNumber + 0.1)
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - 0.1)
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber - 0.2)
            answer3 = (answer3Number).toFixed(2)
        }
        else if (operator === "-") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) - (taskNumber2Fixed * 100)) / 100
            correctAnswer = (correctAnswerNumber).toFixed(2)
            let answer1Number = (correctAnswerNumber + 0.1)
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - 0.1)
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber + 0.2)
            answer3 = (answer3Number).toFixed(2)
        }
        else if (operator === "*") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) * (taskNumber2Fixed * 100)) / 10000
            correctAnswer = (correctAnswerNumber).toFixed(3)
            let answer1Number = (correctAnswerNumber + 0.01)
            answer1 = (answer1Number).toFixed(3)
            let answer2Number = (correctAnswerNumber - 0.01)
            answer2 = (answer2Number).toFixed(3)
            let answer3Number = (correctAnswerNumber - 0.02)
            answer3 = (answer3Number).toFixed(3)
        }
        else if (operator === "/") {
            let correctAnswerNumber = ((taskNumber1Fixed * 100) / (taskNumber2Fixed * 100));
            correctAnswer = (correctAnswerNumber).toFixed(3);
            let answer1Number = (correctAnswerNumber + 0.01);
            answer1 = (answer1Number).toFixed(3);
            let answer2Number = (correctAnswerNumber - 0.01);
            answer2 = (answer2Number).toFixed(3);
            let answer3Number = (correctAnswerNumber + 0.02);
            answer3 = (answer3Number).toFixed(3);
        }
        else if (operator === "^") {

            taskNumber1 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(1);
            taskNumber2 = (Math.random() * (this.exponentRangeMax - this.exponentRangeMin) + this.exponentRangeMin).toFixed(1);

            if (taskNumber1 >= 0) {

                while (taskNumber1 === 0) {
                    taskNumber1 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(1);
                }
                let taskNumber1Fixed = Number(taskNumber1)
                let taskNumber2Fixed = Number(taskNumber2)

                if (taskNumber2Fixed < -1) {
                    let correctAnswerNumber = Math.pow(taskNumber1Fixed, taskNumber2Fixed)
                    correctAnswer = (correctAnswerNumber).toFixed(8)
                    let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                    answer1 = (answer1Number).toFixed(8)
                    let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                    answer2 = (answer2Number).toFixed(8)
                    let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                    answer3 = (answer3Number).toFixed(8)
                }
                else {
                    let correctAnswerNumber = Math.pow(taskNumber1Fixed, taskNumber2Fixed)
                    correctAnswer = (correctAnswerNumber).toFixed(2)
                    let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                    answer1 = (answer1Number).toFixed(2)
                    let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                    answer2 = (answer2Number).toFixed(2)
                    let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                    answer3 = (answer3Number).toFixed(2)
                }
            }
            else {
                while ((taskNumber2 * 10) % 2 !== 0) {   //to prevent (-x)^y - y to be odd number
                    taskNumber2 = Math.floor(Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin)
                }
                let taskNumber1Fixed = Number(taskNumber1)
                let taskNumber2Fixed = Number(taskNumber2)

                if (taskNumber2Fixed < -1) {
                    let correctAnswerNumber = Math.pow(taskNumber1Fixed, taskNumber2Fixed)
                    correctAnswer = (correctAnswerNumber).toFixed(8)
                    let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                    answer1 = (answer1Number).toFixed(8)
                    let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                    answer2 = (answer2Number).toFixed(8)
                    let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                    answer3 = (answer3Number).toFixed(8)
                }
                else {
                    let correctAnswerNumber = Math.pow(taskNumber1Fixed, taskNumber2Fixed)
                    correctAnswer = (correctAnswerNumber).toFixed(2)
                    let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
                    answer1 = (answer1Number).toFixed(2)
                    let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
                    answer2 = (answer2Number).toFixed(2)
                    let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
                    answer3 = (answer3Number).toFixed(2)
                }
            }
        }

        else if (operator === "√") {
            taskNumber1 = (Math.random() * (this.exponentRangeMax - this.exponentRangeMin) + this.exponentRangeMin).toFixed(1)
            taskNumber2 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(1)

            while (taskNumber1 === 0 || taskNumber2 <= 0) {
                taskNumber1 = (Math.random() * (this.exponentRangeMax - this.exponentRangeMin) + this.exponentRangeMin).toFixed(1)
                taskNumber2 = (Math.random() * (this.rangeMax - this.rangeMin) + this.rangeMin).toFixed(1)
            }
            let taskNumber1Fixed = Number(taskNumber1)
            let taskNumber2Fixed = Number(taskNumber2)

            let correctAnswerNumber = Math.pow(taskNumber2Fixed, (1 / taskNumber1Fixed))
            correctAnswer = (correctAnswerNumber).toFixed(2)
            let answer1Number = (correctAnswerNumber + (correctAnswerNumber / 20))
            answer1 = (answer1Number).toFixed(2)
            let answer2Number = (correctAnswerNumber - (correctAnswerNumber / 20))
            answer2 = (answer2Number).toFixed(2)
            let answer3Number = (correctAnswerNumber - (correctAnswerNumber / 40))
            answer3 = (answer3Number).toFixed(2)
        }
        answers.push(answer1, answer2, answer3, correctAnswer);

        answers.sort(() => { return 0.5 - Math.random() });

        this.setState({
            taskNumber1: taskNumber1,
            taskNumber2: taskNumber2,
            answers: answers,
            correctAnswer: correctAnswer
        })
    }

    restartGame = () => {
        this.setState({
            time: 0,
            answers: [],
            operations: [],
            operator: "",
            buttonStatus: "START GAME",
            taskNumber1: "",
            taskNumber2: "",
            messageZindex: -1,
            messageText: "",
            scoreCorrectAnswers: 0,
            scoreWrongAnswers: 0,
            scoreTotal: 0
        })
        this.operations = [];
        clearInterval(this.timer)
        this.props.clearOperations()
    }

    gameTimer = () => {
        this.setState({
            time: this.state.time - 1
        })
        if (this.state.time === 0) {
            clearInterval(this.timer);
            this.props.updatePlayerPoints(this.state.scoreTotal)

            this.setState({
                time: 0,
                answers: [],
                operations: [],
                operator: "",
                taskNumber1: "",
                taskNumber2: "",
                buttonStatus: "PLAY AGAIN",
                messageZindex: 1,
                messageText: ["GAME OVER", `Correct Answers: ${this.state.scoreCorrectAnswers}`,
                    `Wrong Answers: ${this.state.scoreWrongAnswers}`, `Total: ${this.state.scoreTotal}`],
                scoreCorrectAnswers: 0,
                scoreWrongAnswers: 0,
                scoreTotal: 0
            })
            this.props.clearOperations()
        }
    }

    render() {

        return (
            <div className='playGameContainer'>
                <Header time={this.props.time} timeState={this.state.time} scoreTotal={this.state.scoreTotal}
                    answerStatus={this.state.answerStatus} />

                <div className="message" style={{ zIndex: this.state.messageZindex }}>
                    <p>{this.state.messageText[0]}</p> <br />
                    <span>{this.state.messageText[1]}</span> <br />
                    <span>{this.state.messageText[2]}</span> <br />
                    <span>{this.state.messageText[3]}</span> <br />
                </div>

                <div className="tasksContainer">
                    <div className="task">
                        <div className="firstNumber" style={{fontSize: this.state.taskFontSize}}>
                            {this.state.taskNumber1}
                        </div>
                        <div className="operator" style={{fontSize: this.state.operatorFontSize}}>
                            {this.state.operator}
                        </div>
                        <div className="secondNumber" style={{fontSize: this.state.taskFontSize}}>
                            {this.state.taskNumber2}
                        </div>
                    </div>
                </div>
                <div className="answersContainer">
                    <button onClick={this.state.answers.length !== 0 ? () => this.playGame(this.state.answers[0]) : () => { }}>{this.state.answers[0]}</button>
                    <button onClick={this.state.answers.length !== 0 ? () => this.playGame(this.state.answers[1]) : () => { }}>{this.state.answers[1]}</button>
                    <button onClick={this.state.answers.length !== 0 ? () => this.playGame(this.state.answers[2]) : () => { }}>{this.state.answers[2]}</button>
                    <button onClick={this.state.answers.length !== 0 ? () => this.playGame(this.state.answers[3]) : () => { }}>{this.state.answers[3]}</button>
                </div>
                <div className="playGameFooter">
                    <button onClick={this.state.time === 0 ? this.startGame : this.restartGame}>{this.state.buttonStatus}</button>
                </div>
                <Info />
            </div>
        );
    };
};

export default PlayGame;
