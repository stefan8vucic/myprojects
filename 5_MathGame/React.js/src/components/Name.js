import React, { useState } from 'react';
import '../App.css';

function Name(props) {

    const [name, setName] = useState("")
    const [spanClass, setSpanClass] = useState("0")

    const handleInput = (event) => {
        if (event.target.value.length <= 12) {
            setSpanClass("")
            let input = event.target.value
            let toUpper = input.slice(0, 1).toUpperCase() + input.slice(1)
            setName(toUpper.trim())
            props.updateName(toUpper.trim())
        }
        else {
            setSpanClass("nameContainerSpan")
            setTimeout(() => setSpanClass(""), 3000)
        }
    }
    return (
        <div className="nameContainer">
            <p>NAME</p>
            <input placeholder="MathGenius" value={name} onChange={handleInput} id="name"></input><br />
            <span className={spanClass}> 12 characters max </span>
        </div>
    )
}

export default Name;