import React from "react"
import '../App.css'

function Time(props) {

    const handletime = (event) => {
        props.handleTime(event.target.value)
    }

    return (
        <div className='timeContainer'>
            <p>TIME</p>
            <select id="time" onChange={handletime}>
                <option value="30">30s</option>
                <option selected value="60">60s</option>
                <option value="90">90s</option>
                <option value="120">120s</option>
            </select>
        </div>
    );
};

export default Time;