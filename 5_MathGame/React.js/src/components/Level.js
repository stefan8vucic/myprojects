import React, { Component } from "react"
import '../App.css'


class Level extends Component {

    state = {
        easy: true,
        medium: false,
        hard: false,
        legend: false
    }

    handleLevel = (level) => {
        if (level === "easy") {
            this.setState({
                easy: true,
                medium: false,
                hard: false,
                legend: false
            })
            this.props.handleLevel(level)
        }
        else if (level === "medium") {
            this.setState({
                easy: false,
                medium: true,
                hard: false,
                legend: false
            })
            this.props.handleLevel(level)
        }
        else if (level === "hard") {
            this.setState({
                easy: false,
                medium: false,
                hard: true,
                legend: false
            })
            this.props.handleLevel(level)
        }
        else if (level === "legend") {
            this.setState({
                easy: false,
                medium: false,
                hard: false,
                legend: true
            })
            this.props.handleLevel(level)
        }

    }
    render() {
        return (
            <div className="levelContainer">
                <p>LEVEL</p>
                <input checked={this.state.easy} type="checkbox" onChange={() => this.handleLevel("easy")}></input><span>EASY</span><br />
                <input checked={this.state.medium} type="checkbox" onChange={() => this.handleLevel("medium")}></input><span>MEDIUM</span><br />
                <input checked={this.state.hard} type="checkbox" onChange={() => this.handleLevel("hard")}></input><span>HARD</span><br />
                <input checked={this.state.legend} type="checkbox" onChange={() => this.handleLevel("legend")}></input><span>LEGEND</span>
            </div>
        );
    };
};

export default Level;
