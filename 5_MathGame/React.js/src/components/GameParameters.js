import React from "react"
import '../App.css'
import Name from './Name'
import Time from './Time'
import Range from './Range'
import Operations from './Operations'
import Level from './Level'

function GameParameters(props){

        return (
            <div className="gameParameters">
                <Name updateName={props.updateName} />
                <Time handleTime={props.handleTime} />
                <Range handleRange={props.handleRange} />
                <Operations handleOperations={props.handleOperations} level={props.level}
                    zIndex={props.zIndex} updateExponentRange={props.updateExponentRange}
                    exponent={props.exponent} />
                <Level handleLevel={props.handleLevel} />
            </div>
        );
    };

export default GameParameters;