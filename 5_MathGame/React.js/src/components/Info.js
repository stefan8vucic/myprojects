import React, { useState } from 'react';
import '../App.css';

function Info() {
    const [infoClass, setInfoClass] = useState("info")

    const renderInfo = () => {
        setInfoClass("showInfo")
    }

    const hideInfo = () => {
        setInfoClass("hideInfo")
    }

    return (
        <div className="infoContainer">
            <span title="Info" onClick={renderInfo}>ⓘ</span>
            <div className={infoClass}>
                <span title="Hide info" onClick={hideInfo}>X</span>
                <h2>INFO</h2>
                <p>Name</p>
                <ul>
                    <li>Player Name - 12 characters max</li>
                </ul>
                <p>Time</p>
                <ul>
                    <li>Game duration - 30, 60, 90 or 120 seconds</li>
                </ul>
                <p>Range</p>
                <ul>
                    <li>Range of task numbers</li>
                    <li>If y√x and x^y operation selected, range "To" number must be &gt;= 2</li>
                </ul>
                <p>Operations</p>
                <ul>
                    <li>At least one must be selected</li>
                    <li>Multiple selection allowed</li>
                </ul>
                <p>Level</p>
                <ul>
                    <p>Easy</p>
                    <ul>
                        <li>Big difference between the offered answers </li>
                        <li>Subtraction result is a positive number</li>
                        <li>Division result is an integer</li>
                        <li>y√x and x^y - not available</li>
                    </ul>
                    <p>Medium</p>
                    <ul>
                        <li>Small difference between the offered answers</li>
                        <li>Division result is an integer</li>
                        <li>y√x and x^y - y = 2</li>
                        <li>√x result is a selected range integer</li>
                    </ul>
                    <p>Hard</p>
                    <ul>
                        <li>Task numbers with one decimal place </li>
                        <li>y√x and x^y - y = integer from chosen range</li>
                    </ul>
                    <p>Legend</p>
                    <ul>
                        <li>Task numbers with two decimal places </li>
                        <li>y√x and x^y - y = one decimal place number from chosen range</li>
                    </ul>
                </ul>
                <p>Ranking</p>
                <ul>
                    <li>Displays played games ranking</li>
                    <li>Rank | Player Name | Score</li>
                </ul>
                <p>Ranking Filter</p>
                <ul>
                    <li>Allows filtering of results by level and time</li>
                </ul>
            </div>
        </div>
    );
}

export default Info;