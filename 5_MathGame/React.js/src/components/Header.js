import React from 'react'
import '../App.css'

function Header(props) {

        return (
            <div className="playGameHeader">
                <div className="timer">
                    <p>Time:</p>{props.timeState === 0 ? props.time : props.timeState > 9 ? props.timeState : `0${props.timeState}`}
                </div>
                <div className="answerStatus">
                    <div className={props.answerStatus}>
                        {props.answerStatus === "correctAnswer" ? "BRAVO" : props.answerStatus === "wrongAnswer" ? "OPS" : ""}
                    </div>
                </div>
                <div className="score">
                    <p>Score:</p>{props.scoreTotal}
                </div>
            </div>
        );
    };

export default Header;