import React, { Component } from 'react';
import '../App.css';


class Ranking extends Component {

    state = {
        filterLevel: "all",
        filterTime: "all"
    }

    filterRankingLevel = (event) => {
        this.setState({
            filterLevel: event.target.value
        })
        this.props.filterRanking(event.target.value, this.state.filterTime)
    }

    filterRankingTime = (event) => {
        this.setState({
            filterTime: event.target.value
        })
        this.props.filterRanking(this.state.filterLevel, event.target.value)
    }

    render() {
        let key = 0
        return (
            <div className="gameRankingContainer">
                <div className="gameRankingHeader">
                    <p>RANKING</p>
                    <span>Filter</span><br />
                Level:
                <select onChange={this.filterRankingLevel} >
                        <option value="all">ALL</option>
                        <option value="easy">EASY</option>
                        <option value="medium">MEDIUM</option>
                        <option value="hard">HARD</option>
                        <option value="legend">LEGEND</option>
                    </select><br />
                    Time:
                    <select onChange={this.filterRankingTime}>
                        <option value="all">ALL</option>
                        <option value="30">30s</option>
                        <option value="60">60s</option>
                        <option value="90">90s</option>
                        <option value="120">120s</option>
                    </select>
                </div>
                <div className="gameRankingBody">
                    {this.props.ranking.map(player => <div  key={player.rank} className="rankingCard">
                        <div className="rankingCardRank">{player.rank}.</div>
                        <div className="rankingCardName">{player.name}</div>
                        <div className="rankingCardScore">{player.score}</div>
                    </div>)}
                </div>
                <div className="gameRankingFooter">
                    <button onClick={this.props.clearRanking}>CLEAR RANKING</button>
                </div>




            </div>
        )
    }
}

export default Ranking;