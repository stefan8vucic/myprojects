import React, { Component } from "react";
import '../App.css';

class Operations extends Component {

    state = {
        addition: false,
        subtraction: false,
        multiplication: false,
        division: false,
        exponent: false,
        exponentRangeMin: 1,
        exponentRangeMax: 5
    }

    handleAddition = (event) => {
        this.setState({
            addition: event.target.checked
        })
        this.props.handleOperations(event.target.checked, this.state.subtraction, this.state.multiplication, this.state.division, this.props.exponent)
    }

    handleSubtraction = (event) => {
        this.setState({
            subtraction: event.target.checked
        })
        this.props.handleOperations(this.state.addition, event.target.checked, this.state.multiplication, this.state.division, this.props.exponent)
    }

    handleMultiplication = (event) => {
        this.setState({
            multiplication: event.target.checked
        })
        this.props.handleOperations(this.state.addition, this.state.subtraction, event.target.checked, this.state.division, this.props.exponent)
    }

    handleDivision = (event) => {
        this.setState({
            division: event.target.checked
        })
        this.props.handleOperations(this.state.addition, this.state.subtraction, this.state.multiplication, event.target.checked, this.props.exponent)
    }

    handleExponent = (event) => {
        this.setState({
            exponent: event.target.checked
        })
        this.props.handleOperations(this.state.addition, this.state.subtraction, this.state.multiplication, this.state.division, event.target.checked)
    }

    handleExponentRangeMin = (event) => {
        this.setState({
            exponentRangeMin: event.target.value
        })
        this.props.updateExponentRange(event.target.value, this.state.exponentRangeMax)
    }

    handleExponentRangeMax = (event) => {
        this.setState({
            exponentRangeMax: event.target.value
        })
        this.props.updateExponentRange(this.state.exponentRangeMin, event.target.value)
    }

    render() {
        return (
            <div className='operationsContainer'>
                <p>OPERATIONS</p>
                <input onChange={this.handleAddition} type='checkbox'></input><span>Addition</span><br />
                <input onChange={this.handleSubtraction} type='checkbox'></input><span>Subtraction</span><br />
                <input onChange={this.handleMultiplication} type='checkbox'></input><span>Multiplication</span><br />
                <input onChange={this.handleDivision} type='checkbox'></input><span>Division</span><br />
                <input onChange={this.handleExponent} type='checkbox' disabled={this.props.level === "easy" ? true : false}
                    checked={this.props.exponent ? true : false}></input><span>y√x and x^y</span><br />
                <div className="exponentRange" style={{ zIndex: this.props.zIndex }}>
                    <span>y range: </span>
                    <input type="number" placeholder="1" value={this.state.exponentRangeMin} id="exponentRangeMin" onChange={this.handleExponentRangeMin}></input>
                    <span id="dash">-</span>
                    <input type="number" placeholder="5" value={this.state.exponentRangeMax} id="exponentRangeMax" onChange={this.handleExponentRangeMax}></input>
                </div>
            </div>
        );
    };
};

export default Operations;