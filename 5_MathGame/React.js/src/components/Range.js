import React, { Component } from "react"
import '../App.css'


class Range extends Component {

    state = {
        rangeMin: 1,
        rangeMax: 10
    }

    handleRangeMin = (event) => {
        this.setState({
            rangeMin: event.target.value
        })
        this.props.handleRange(event.target.value, this.state.rangeMax)
    }

    handleRangeMax = (event) => {
        this.setState({
            rangeMax: event.target.value
        })
        this.props.handleRange(this.state.rangeMin, event.target.value)
    }

    render() {
        return (
            <div className='rangeContainer '>
                <p>RANGE</p>
                <span>From:</span>
                <input placeholder="1" value={this.state.rangeMin} id="rangeMin" onChange={this.handleRangeMin} type="number"></input><br />
                <span>To:</span>
                <input placeholder="10" value={this.state.rangeMax} id="rangeMax" onChange={this.handleRangeMax} type="number"></input>
            </div>
        );
    };
};

export default Range;