import React from 'react';
import './App.css';
import GameParameters from './components/GameParameters';
import PlayGame from './components/PlayGame';
import Ranking from './components/Ranking';


class App extends React.Component {

  state = {
    time: "60",
    rangeMin: 1,
    rangeMax: 10,
    level: "easy",
    addition: false,
    subtraction: false,
    multiplication: false,
    division: false,
    exponent: false,
    exponentRangezIndex: -1,
    exponentRangeMin: 1,
    exponentRangeMax: 5,
    background: require('./Photos/easy.jpg'),
    player: {
      rank: 0,
      name: "MathGenius",
      level: "easy",
      time: "60",
      score: 0,
    },
    allResults: [],
    filteredResults: [],
    filterLevel: "all",
    filterTime: "all",
  }

  operations = []

  updateName = (name) => {
    let player = this.state.player;
    player.name = name;

    this.setState({
      player: player
    })
  }

  handleTime = (time) => {
    let player = this.state.player;
    player.time = time;

    this.setState({
      time: time,
      player: player
    })
  }

  handleRange = (rangeMin, rangeMax) => {
    this.setState({
      rangeMin: Number(rangeMin),
      rangeMax: Number(rangeMax)
    })
  }

  handleOperations = (addition, subtraction, multiplication, division, exponent) => {
    this.setState({
      addition: addition,
      subtraction: subtraction,
      multiplication: multiplication,
      division: division,
      exponent: exponent
    })

    if (exponent) {
      if (this.state.level === "hard" || this.state.level === "legend") {
        this.setState({
          exponentRangezIndex: 1
        })
      }
    }
    else {
      this.setState({
        exponentRangezIndex: -1
      })
    }
  }

  updateExponentRange = (exponentRangeMin, exponentRangeMax) => {
    this.setState({
      exponentRangeMin: Number(exponentRangeMin),
      exponentRangeMax: Number(exponentRangeMax)
    })
  }

  handleLevel = (level) => {
    let player = this.state.player;
    player.level = level

    this.setState({
      level: level,
      player: player
    })
    this.setBackground(level)

    if (level === "hard" || level === "legend") {
      if (this.state.exponent) {
        this.setState({
          exponentRangezIndex: 1
        })
      }
    }
    else if (level === "medium") {
      this.setState({
        exponentRangezIndex: -1,
      })
    }
    else {
      this.setState({
        exponentRangezIndex: -1,
        exponent: false
      })
    }
  }

  updateOperations = () => {

    if (this.state.addition) {
      this.operations.push("+")
    }
    if (this.state.subtraction) {
      this.operations.push("-")
    }
    if (this.state.multiplication) {
      this.operations.push("*")
    }
    if (this.state.division) {
      this.operations.push("/")
    }
    if (this.state.exponent) {
      this.operations.push("^", "√")
    }
  }

  clearOperations = () => {
    this.operations = [];
  }

  setBackground = (level) => {
    if (level === 'easy') {
      this.setState({
        background: require('./Photos/easy.jpg')
      })
    }
    else if (level === 'medium') {
      this.setState({
        background: require('./Photos/medium.jpg')
      })
    }
    else if (level === 'hard') {
      this.setState({
        background: require('./Photos/hard.jpg')
      })
    }
    else if (level === 'legend') {
      this.setState({
        background: require('./Photos/legend.jpeg')
      })
    }
  }

  updatePlayerPoints = (score) => {      
    let playerObject = { ...this.state.player }
    playerObject.score = score;

    let allResults = this.state.allResults
    allResults.push(playerObject)
    allResults.sort((player1, player2) => player2.score - player1.score)

    this.setState({
      allResults: allResults,    //update allReasults array
    })

    this.filterRanking(this.state.filterLevel, this.state.filterTime)
  }

  filterRanking = (level, time) => {
    if (level === "all" && time === "all") {
      let rank = 0;
      let results = this.state.allResults
      results.forEach(player => { rank++; player.rank = rank })
      this.setState({
        filteredResults: results,
        filterTime: "all",
        filterLevel: "all"
      })
    }
    else if (level !== "all" && time === "all") {
      let rank = 0;
      let results = this.state.allResults
      let filteredResults = results.filter(player => player.level === level)
      filteredResults.forEach(player => { rank++; player.rank = rank })
      this.setState({
        filteredResults: filteredResults,
        filterLevel: level,
        filterTime: "all"
      })
    }
    else if (level === "all" && time !== "all") {
      let rank = 0;
      let results = this.state.allResults
      let filteredResults = results.filter(player => player.time === time)
      filteredResults.forEach(player => { rank++; player.rank = rank })
      this.setState({
        filteredResults: filteredResults,
        filterTime: time,
        filterLevel: "all"
      })
    }
    else if (level !== "all" && time !== "all") {
      let rank = 0;
      let results = this.state.allResults
      let filteredResults = results.filter(player => player.level === level && player.time === time)
      filteredResults.forEach(player => { rank++; player.rank = rank })
      this.setState({
        filteredResults: filteredResults,
        filterTime: time,
        filterLevel: level
      })
    }
  }

  clearRanking = () => {
    this.setState({
      allResults: [],
      filteredResults: []
    })
  }

  render() {
    return (
      <div className="container" style={{ backgroundImage: 'url(' + this.state.background + ')' }}>
        <div className="gameContainer">
          <GameParameters handleLevel={this.handleLevel} handleTime={this.handleTime} handleRange={this.handleRange}
            handleOperations={this.handleOperations} level={this.state.level} updateName={this.updateName}
            zIndex={this.state.exponentRangezIndex} updateExponentRange={this.updateExponentRange}
            exponent={this.state.exponent} />
          <PlayGame time={this.state.time} operations={this.operations}
            parameters={this.state} updateOperations={this.updateOperations}
            clearOperations={this.clearOperations} updatePlayerPoints={this.updatePlayerPoints} />
          <Ranking ranking={this.state.filteredResults} clearRanking={this.clearRanking}
            filterRanking={this.filterRanking}
          />
        </div>
      </div>
    );
  }
}

export default App;
